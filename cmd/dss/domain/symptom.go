package domain

import (
	"errors"
	"github.com/go-openapi/strfmt"
	"time"
)

const (
	// SymptomsCollectionName - is document collection name
	SymptomsCollectionName = "symptoms"
)

type Symptom struct {
	Code        strfmt.UUID `bson:"_id,omitempty"`
	Condition   interface{} `bson:"condition,omitempty"` // jsonLogic
	Description *string     `bson:"description,omitempty"`
	Created     time.Time   `bson:"created_at,omitempty"`
	Updated     *time.Time  `bson:"updated_at,omitempty"`
}

type SymptomsFilter struct {
	Limit  int64
	Offset int64
}

var (
	ErrSymptomNotFound      = errors.New("symptom not found")
	ErrSymptomAlreadyExists = errors.New("symptom already exist")
)
