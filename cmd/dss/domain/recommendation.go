package domain

import (
	"errors"
	"github.com/go-openapi/strfmt"
	"time"
)

const (
	// RecommendationsCollectionName - is document collection name
	RecommendationsCollectionName = "recommendations"
)

type Recommendation struct {
	Code        strfmt.UUID `bson:"_id,omitempty"`
	GroupCode   string      `bson:"group_code,omitempty"`
	Condition   interface{} `bson:"condition,omitempty"` // jsonLogic
	Description *string     `bson:"description,omitempty"`
	Created     time.Time   `bson:"created_at,omitempty"`
	Updated     *time.Time  `bson:"updated_at,omitempty"`
}

type RecommendationsFilter struct {
	Limit     int64
	Offset    int64
	GroupCode string `bson:"group_code,omitempty"`
}

var (
	ErrRecommendationNotFound      = errors.New("recommendation not found")
	ErrRecommendationAlreadyExists = errors.New("recommendation already exist")
	ErrInvalidConditionRule        = errors.New("invalid condition json logic rule")
)
