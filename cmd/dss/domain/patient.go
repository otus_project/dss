package domain

import (
	"errors"
	"github.com/go-openapi/strfmt"
	"time"
)

const (
	// PatientsCollectionName - is document collection name
	PatientsCollectionName = "patients"
)

type Patient struct {
	GUID       strfmt.UUID `bson:"_id,omitempty"`
	BirthDate  time.Time   `bson:"birth_date,omitempty"`
	CkdCode    string      `bson:"ckd_code,omitempty"`
	GenderCode string      `bson:"gender_code,omitempty"`
	Created    time.Time   `bson:"created_at,omitempty"`
	Updated    *time.Time  `bson:"updated_at,omitempty"`
}

type PatientsFilter struct {
	Object Patient
	Limit  int64
	Offset int64
}

type MatchData struct {
	Patient struct {
		GenderCode string          `json:"genderCode"`
		BirthDate  string          `json:"birthDate"`
		CkdCode    string          `json:"ckdCode"`
		Params     MatchDataParams `json:"params"`
	} `json:"patient"`
}

type MatchDataParams struct {
	TypeCode string      `json:"typeCode"`
	Data     interface{} `json:"data"`
	Created  time.Time   `json:"created_at"`
	Updated  *time.Time  `json:"updated_at"`
}

var (
	ErrPatientNotFound      = errors.New("patient not found")
	ErrPatientAlreadyExists = errors.New("patient already exist")
	ErrInvalidResultType    = errors.New("result must be convertible to bool type")
	ErrParamsRuleMismatch   = errors.New("params do not match with the rules")
)
