package domain

import (
	"errors"
	"github.com/go-openapi/strfmt"
	"time"
)

const (
	// PatientParamsCollectionName - is document collection name
	PatientParamsCollectionName = "patient-params"
)

type PatientParam struct {
	GUID        strfmt.UUID `bson:"_id,omitempty"`
	PatientGUID strfmt.UUID `bson:"patient_GUID,omitempty"`
	TypeCode    string      `bson:"type_code,omitempty"`
	Data        interface{} `bson:"data,omitempty"`
	Created     time.Time   `bson:"created_at,omitempty"`
	Updated     *time.Time  `bson:"updated_at,omitempty"`
}

type PatientParamsFilter struct {
	Object PatientParam
	Limit  int64
	Offset int64
}

var (
	ErrPatientDataNotFound      = errors.New("patient param not found")
	ErrPatientDataAlreadyExists = errors.New("patient param already exist")
	ErrRelatedPatientNotFound   = errors.New("related patient not found")
)
