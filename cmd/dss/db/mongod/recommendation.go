package mongod

import (
	"context"
	"fmt"
	"github.com/go-openapi/strfmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gitlab.com/otus_project/dss/cmd/dss/domain"
	"gitlab.com/otus_project/dss/db/mongod"
	"strings"
	"time"
)

type RecommendationRepository interface {
	Set(domain.Recommendation) (*domain.Recommendation, error)
	Get(domain.Recommendation) (*domain.Recommendation, error)
	Update(domain.Recommendation, domain.Recommendation) (*domain.Recommendation, error)
	Delete(domain.Recommendation) error
	List(domain.RecommendationsFilter) ([]domain.Recommendation, error)
}

type recommendRepo struct {
	ctx context.Context
	db  *mongo.Collection
}

func NewRecommendationRepo(store mongod.Storer) RecommendationRepository {
	return &recommendRepo{
		context.Background(),
		store.Collection(domain.RecommendationsCollectionName),
	}
}

func (p *recommendRepo) Set(filter domain.Recommendation) (resp *domain.Recommendation, err error) {
	ctx, _ := context.WithTimeout(p.ctx, 1*time.Second)

	filter.Created, err = time.Parse(domain.TimeFormat, time.Now().Format(domain.TimeFormat))
	if err != nil {
		return nil, err
	}
	resp = &filter

	res, err := p.db.InsertOne(ctx, filter)
	if err != nil {
		if strings.Contains(err.Error(), "duplicate key") {
			return nil, domain.ErrRecommendationAlreadyExists
		}
		return nil, err
	}

	var uuid strfmt.UUID
	switch res.InsertedID.(type) {
	case string:
		err = uuid.Scan(res.InsertedID)
	case primitive.ObjectID:
		err = uuid.Scan(
			fmt.Sprintf("%q", res.InsertedID.(primitive.ObjectID).Hex()),
		)
	}
	if err != nil {
		return nil, err
	}

	resp.Code = uuid

	return resp, nil
}

func (p *recommendRepo) Get(filter domain.Recommendation) (_ *domain.Recommendation, err error) {
	ctx, _ := context.WithTimeout(p.ctx, 1*time.Second)

	var result domain.Recommendation
	if err := p.db.FindOne(ctx, filter).Decode(&result); err != nil {

		if err == mongo.ErrNoDocuments {
			return nil, domain.ErrRecommendationNotFound
		}

		return nil, err
	}

	doc, ok := result.Condition.(primitive.D)
	if ok {
		result.Condition = doc.Map()
	}

	return &result, nil

}

func (p *recommendRepo) Update(
	filter domain.Recommendation,
	update domain.Recommendation,
) (resp *domain.Recommendation, err error) {
	ctx, _ := context.WithTimeout(p.ctx, 1*time.Second)

	tm, err := time.Parse(domain.TimeFormat, time.Now().Format(domain.TimeFormat))
	if err != nil {
		return nil, err
	}
	update.Updated = &tm

	opts := new(options.FindOneAndUpdateOptions)
	opts.SetUpsert(false)
	opts.SetReturnDocument(options.After)

	err = p.db.FindOneAndUpdate(ctx, filter, bson.D{{"$set", update}}, opts).Decode(&resp)
	if err != nil {

		if err == mongo.ErrNoDocuments {
			return nil, domain.ErrRecommendationNotFound
		}

		return nil, err
	}

	switch d := resp.Condition.(type) {
	case primitive.D:
		resp.Condition = d.Map()
	case primitive.A:
		var bigD primitive.D
		for _, v := range d {
			bigD = append(bigD, v.(primitive.D)...)
		}
		resp.Condition = bigD.Map()
	}
	return resp, nil
}

func (p *recommendRepo) Delete(filter domain.Recommendation) error {
	ctx, _ := context.WithTimeout(p.ctx, 1*time.Second)

	err := p.db.FindOneAndDelete(ctx, bson.D{{"_id", filter.Code}}, &options.FindOneAndDeleteOptions{}).Err()
	if err != nil {

		if err == mongo.ErrNoDocuments {
			return domain.ErrRecommendationNotFound
		}

		return err
	}

	return nil
}

func (p *recommendRepo) List(filter domain.RecommendationsFilter) (_ []domain.Recommendation, err error) {
	var recs = make([]domain.Recommendation, 0)
	ctx, _ := context.WithTimeout(p.ctx, 5*time.Second)

	opts := new(options.FindOptions)
	opts.SetLimit(filter.Limit)
	opts.SetSkip(filter.Offset)

	var doc = bson.D{{}}
	if filter.GroupCode != "" {
		doc = bson.D{{"group_code", filter.GroupCode}}
	}

	cur, err := p.db.Find(ctx, doc, opts)
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)

	for cur.Next(ctx) {
		var result domain.Recommendation
		if err = cur.Decode(&result); err != nil {
			return nil, err
		}
		switch d := result.Condition.(type) {
		case primitive.D:
			result.Condition = d.Map()
		case primitive.A:
			var bigD primitive.D
			for _, v := range d {
				bigD = append(bigD, v.(primitive.D)...)
			}
			result.Condition = bigD.Map()
		}
		recs = append(recs, result)
	}
	if err := cur.Err(); err != nil {
		return nil, err
	}

	if len(recs) == 0 {
		return recs, domain.ErrRecommendationNotFound
	}

	return recs, nil
}
