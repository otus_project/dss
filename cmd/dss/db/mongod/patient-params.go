package mongod

import (
	"context"
	"github.com/go-openapi/strfmt"
	"github.com/gofrs/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gitlab.com/otus_project/dss/cmd/dss/domain"
	"gitlab.com/otus_project/dss/db/mongod"
	"strings"
	"time"
)

type PatientParamsRepository interface {
	Set(domain.PatientParam) (*domain.PatientParam, error)
	Get(domain.PatientParam) (*domain.PatientParam, error)
	Update(domain.PatientParam, domain.PatientParam) (*domain.PatientParam, error)
	Delete(domain.PatientParam) error
	List(domain.PatientParamsFilter) ([]domain.PatientParam, error)
}

type patientParamsRepo struct {
	ctx context.Context
	db  *mongo.Collection
}

func NewPatientParamsRepo(store mongod.Storer) PatientParamsRepository {
	return &patientParamsRepo{
		context.Background(),
		store.Collection(domain.PatientParamsCollectionName),
	}
}

func (p *patientParamsRepo) Set(filter domain.PatientParam) (resp *domain.PatientParam, err error) {
	ctx, _ := context.WithTimeout(p.ctx, 1*time.Second)

	filter.Created, err = time.Parse(domain.TimeFormat, time.Now().Format(domain.TimeFormat))
	if err != nil {
		return nil, err
	}
	var guid strfmt.UUID
	err = guid.Scan(uuid.Must(uuid.NewV1()).String())
	if err != nil {
		return nil, err
	}

	filter.GUID = guid
	resp = &filter

	_, err = p.db.InsertOne(ctx, filter)
	if err != nil {
		if strings.Contains(err.Error(), "duplicate key") {
			return nil, domain.ErrPatientDataAlreadyExists
		}
		return nil, err
	}

	return resp, nil
}

func (p *patientParamsRepo) Get(filter domain.PatientParam) (_ *domain.PatientParam, err error) {
	ctx, _ := context.WithTimeout(p.ctx, 1*time.Second)

	var result domain.PatientParam
	if err := p.db.FindOne(ctx, filter).Decode(&result); err != nil {

		if err == mongo.ErrNoDocuments {
			return nil, domain.ErrPatientDataNotFound
		}

		return nil, err
	}

	doc, ok := result.Data.(primitive.D)
	if ok {
		result.Data = doc.Map()
		return
	}

	return &result, nil

}

func (p *patientParamsRepo) Update(
	filter domain.PatientParam,
	update domain.PatientParam,
) (resp *domain.PatientParam, err error) {
	ctx, _ := context.WithTimeout(p.ctx, 1*time.Second)

	tm, err := time.Parse(domain.TimeFormat, time.Now().Format(domain.TimeFormat))
	if err != nil {
		return nil, err
	}
	update.Updated = &tm

	opts := new(options.FindOneAndUpdateOptions)
	opts.SetUpsert(false)
	opts.SetReturnDocument(options.After)

	err = p.db.FindOneAndUpdate(ctx, filter, bson.D{{"$set", update}}, opts).Decode(&resp)
	if err != nil {

		if err == mongo.ErrNoDocuments {
			return nil, domain.ErrPatientDataNotFound
		}

		return nil, err
	}

	doc, ok := resp.Data.(primitive.D)
	if ok {
		resp.Data = doc.Map()
	}
	return resp, nil
}

func (p *patientParamsRepo) Delete(filter domain.PatientParam) error {
	ctx, _ := context.WithTimeout(p.ctx, 1*time.Second)

	err := p.db.FindOneAndDelete(ctx, bson.D{{"_id", filter.GUID}}, &options.FindOneAndDeleteOptions{}).Err()
	if err != nil {

		if err == mongo.ErrNoDocuments {
			return domain.ErrPatientDataNotFound
		}

		return err
	}

	return nil
}

func (p *patientParamsRepo) List(filter domain.PatientParamsFilter) (_ []domain.PatientParam, err error) {
	var dataset = make([]domain.PatientParam, 0)
	ctx, _ := context.WithTimeout(p.ctx, 5*time.Second)

	opts := new(options.FindOptions)
	opts.SetLimit(filter.Limit)
	opts.SetSkip(filter.Offset)

	cur, err := p.db.Find(ctx, filter.Object, opts)
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)

	for cur.Next(ctx) {
		var result domain.PatientParam
		if err = cur.Decode(&result); err != nil {
			return nil, err
		}

		switch d := result.Data.(type) {
		case primitive.D:
			result.Data = d.Map()
		case primitive.A:
			var bigD primitive.D
			for _, v := range d {
				bigD = append(bigD, v.(primitive.D)...)
			}
			result.Data = bigD.Map()
		}
		dataset = append(dataset, result)
	}
	if err := cur.Err(); err != nil {
		return nil, err
	}

	if len(dataset) == 0 {
		return dataset, domain.ErrPatientDataNotFound
	}

	return dataset, nil
}
