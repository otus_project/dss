package mongod

import (
	"context"
	"fmt"
	"github.com/go-openapi/strfmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gitlab.com/otus_project/dss/cmd/dss/domain"
	"gitlab.com/otus_project/dss/db/mongod"
	"strings"
	"time"
)

type SymptomRepository interface {
	Set(domain.Symptom) (*domain.Symptom, error)
	Get(domain.Symptom) (*domain.Symptom, error)
	Update(domain.Symptom, domain.Symptom) (*domain.Symptom, error)
	Delete(domain.Symptom) error
	List(domain.SymptomsFilter) ([]domain.Symptom, error)
}

type symptomRepo struct {
	ctx context.Context
	db  *mongo.Collection
}

func NewSymptomRepo(store mongod.Storer) SymptomRepository {
	return &symptomRepo{
		context.Background(),
		store.Collection(domain.SymptomsCollectionName),
	}
}

func (p *symptomRepo) Set(filter domain.Symptom) (resp *domain.Symptom, err error) {
	ctx, _ := context.WithTimeout(p.ctx, 1*time.Second)

	filter.Created, err = time.Parse(domain.TimeFormat, time.Now().Format(domain.TimeFormat))
	if err != nil {
		return nil, err
	}
	resp = &filter

	res, err := p.db.InsertOne(ctx, filter)
	if err != nil {
		if strings.Contains(err.Error(), "duplicate key") {
			return nil, domain.ErrSymptomAlreadyExists
		}
		return nil, err
	}

	var uuid strfmt.UUID
	switch res.InsertedID.(type) {
	case string:
		err = uuid.Scan(res.InsertedID)
	case primitive.ObjectID:
		err = uuid.Scan(
			fmt.Sprintf("%q", res.InsertedID.(primitive.ObjectID).Hex()),
		)
	}
	if err != nil {
		return nil, err
	}

	resp.Code = uuid

	return resp, nil
}

func (p *symptomRepo) Get(filter domain.Symptom) (_ *domain.Symptom, err error) {
	var result domain.Symptom
	ctx, _ := context.WithTimeout(p.ctx, 1*time.Second)
	if err := p.db.FindOne(ctx, filter).Decode(&result); err != nil {

		if err == mongo.ErrNoDocuments {
			return nil, domain.ErrSymptomNotFound
		}

		return nil, err
	}

	doc, ok := result.Condition.(primitive.D)
	if ok {
		result.Condition = doc.Map()
	}

	return &result, nil
}

func (p *symptomRepo) Update(
	filter domain.Symptom,
	update domain.Symptom,
) (resp *domain.Symptom, err error) {
	ctx, _ := context.WithTimeout(p.ctx, 1*time.Second)

	tm, err := time.Parse(domain.TimeFormat, time.Now().Format(domain.TimeFormat))
	if err != nil {
		return nil, err
	}
	update.Updated = &tm

	opts := new(options.FindOneAndUpdateOptions)
	opts.SetUpsert(false)
	opts.SetReturnDocument(options.After)

	err = p.db.FindOneAndUpdate(ctx, filter, bson.D{{"$set", update}}, opts).Decode(&resp)
	if err != nil {

		if err == mongo.ErrNoDocuments {
			return nil, domain.ErrSymptomNotFound
		}

		return nil, err
	}

	switch d := resp.Condition.(type) {
	case primitive.D:
		resp.Condition = d.Map()
	case primitive.A:
		var bigD primitive.D
		for _, v := range d {
			bigD = append(bigD, v.(primitive.D)...)
		}
		resp.Condition = bigD.Map()
	}
	return resp, nil
}

func (p *symptomRepo) Delete(filter domain.Symptom) error {
	ctx, _ := context.WithTimeout(p.ctx, 1*time.Second)

	err := p.db.FindOneAndDelete(ctx, bson.D{{"_id", filter.Code}}, &options.FindOneAndDeleteOptions{}).Err()
	if err != nil {

		if err == mongo.ErrNoDocuments {
			return domain.ErrSymptomNotFound
		}

		return err
	}

	return nil
}

func (p *symptomRepo) List(filter domain.SymptomsFilter) (_ []domain.Symptom, err error) {
	var symptoms = make([]domain.Symptom, 0)
	ctx, _ := context.WithTimeout(p.ctx, 5*time.Second)

	opts := new(options.FindOptions)
	opts.SetLimit(filter.Limit)
	opts.SetSkip(filter.Offset)
	cur, err := p.db.Find(ctx, bson.D{}, opts)
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)

	for cur.Next(ctx) {
		var result domain.Symptom
		if err = cur.Decode(&result); err != nil {
			return nil, err
		}
		switch d := result.Condition.(type) {
		case primitive.D:
			result.Condition = d.Map()
		case primitive.A:
			var bigD primitive.D
			for _, v := range d {
				bigD = append(bigD, v.(primitive.D)...)
			}
			result.Condition = bigD.Map()
		}
		symptoms = append(symptoms, result)
	}
	if err := cur.Err(); err != nil {
		return nil, err
	}

	if len(symptoms) == 0 {
		return symptoms, domain.ErrSymptomNotFound
	}

	return symptoms, nil
}
