package mongod

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/go-openapi/strfmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/otus_project/dss/cmd/dss/domain"
	"gitlab.com/otus_project/dss/db/mongod"
)

type PatientRepository interface {
	Set(domain.Patient) (*domain.Patient, error)
	Get(domain.Patient) (*domain.Patient, error)
	Update(domain.Patient, domain.Patient) (*domain.Patient, error)
	Delete(domain.Patient) error
	List(domain.PatientsFilter) ([]domain.Patient, error)
}

type patientRepo struct {
	ctx context.Context
	db  *mongo.Collection
}

func NewPatientRepo(store mongod.Storer) PatientRepository {
	return &patientRepo{
		context.Background(),
		store.Collection(domain.PatientsCollectionName),
	}
}

func (p *patientRepo) Set(filter domain.Patient) (resp *domain.Patient, err error) {
	ctx, _ := context.WithTimeout(p.ctx, 1*time.Second)

	filter.Created, err = time.Parse(domain.TimeFormat, time.Now().Format(domain.TimeFormat))
	if err != nil {
		return nil, err
	}
	resp = &filter

	res, err := p.db.InsertOne(ctx, filter)
	if err != nil {
		if strings.Contains(err.Error(), "duplicate key") {
			return nil, domain.ErrPatientAlreadyExists
		}
		return nil, err
	}

	var uuid strfmt.UUID
	switch res.InsertedID.(type) {
	case string:
		err = uuid.Scan(res.InsertedID)
	case primitive.ObjectID:
		err = uuid.Scan(
			fmt.Sprintf("%q", res.InsertedID.(primitive.ObjectID).Hex()),
		)
	}
	if err != nil {
		return nil, err
	}

	resp.GUID = uuid

	return resp, nil
}

func (p *patientRepo) Get(filter domain.Patient) (_ *domain.Patient, err error) {
	var result domain.Patient
	ctx, _ := context.WithTimeout(p.ctx, 1*time.Second)
	if err := p.db.FindOne(ctx, filter).Decode(&result); err != nil {

		if err == mongo.ErrNoDocuments {
			return nil, domain.ErrPatientNotFound
		}

		return nil, err
	}
	return &result, nil

}

func (p *patientRepo) Update(
	filter domain.Patient,
	update domain.Patient,
) (resp *domain.Patient, err error) {
	ctx, _ := context.WithTimeout(p.ctx, 1*time.Second)

	tm, err := time.Parse(domain.TimeFormat, time.Now().Format(domain.TimeFormat))
	if err != nil {
		return nil, err
	}
	update.Updated = &tm

	opts := new(options.FindOneAndUpdateOptions)
	opts.SetUpsert(false)
	opts.SetReturnDocument(options.After)

	err = p.db.FindOneAndUpdate(ctx, filter, bson.D{{"$set", update}}, opts).Decode(&resp)
	if err != nil {

		if err == mongo.ErrNoDocuments {
			return nil, domain.ErrPatientNotFound
		}

		return nil, err
	}

	return resp, nil
}

func (p *patientRepo) Delete(filter domain.Patient) error {
	ctx, _ := context.WithTimeout(p.ctx, 1*time.Second)

	err := p.db.FindOneAndDelete(ctx, bson.D{{"_id", filter.GUID}}, &options.FindOneAndDeleteOptions{}).Err()

	if err == mongo.ErrNoDocuments {
		return domain.ErrPatientNotFound
	}

	return err
}

func (p *patientRepo) List(filter domain.PatientsFilter) (_ []domain.Patient, err error) {
	var patients = make([]domain.Patient, 0)
	ctx, _ := context.WithTimeout(p.ctx, 5*time.Second)

	opts := new(options.FindOptions)
	opts.SetLimit(filter.Limit)
	opts.SetSkip(filter.Offset)
	cur, err := p.db.Find(ctx, filter.Object, opts)
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)

	for cur.Next(ctx) {
		var result domain.Patient
		if err = cur.Decode(&result); err != nil {
			return nil, err
		}
		patients = append(patients, result)
	}
	if err := cur.Err(); err != nil {
		return nil, err
	}

	if len(patients) == 0 {
		return patients, domain.ErrPatientNotFound
	}

	return patients, nil
}
