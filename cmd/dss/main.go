package main

import (
	"context"
	"fmt"
	"github.com/getsentry/raven-go"
	"github.com/go-openapi/loads"
	"log"
	"os"
	"gitlab.com/otus_project/dss/db/mongod"
	"gitlab.com/otus_project/dss/logger"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/otus_project/dss/api/restapi"
	"gitlab.com/otus_project/dss/api/restapi/operations"
)

var (
	version = "No Version Provided"
	cfgFile string
)

var cmd = &cobra.Command{
	Use:     "dss",
	Short:   "dss service",
	Long:    `Just use it`,
	Version: version,
	PreRun: func(cmd *cobra.Command, args []string) {
		var mandatoryParams = []string{
			"http.host",
			"http.port",
		}
		var missing []string

		for _, param := range mandatoryParams {
			if viper.Get(param) == "" || viper.Get(param) == 0 {
				missing = append(missing, param)
			}
		}

		if len(missing) != 0 {
			log.Fatalln("Missed mandatory params: ", missing, ". Use --help flag or config")
		}
	},
	Run: func(cmd *cobra.Command, args []string) {
		// Logging init
		l := logger.NewLogger(
			setOutput(),
			viper.GetString("logging.level"),
			viper.GetString("logging.format"),
		)
		l.Core().DSSInfo(
			"", "", "", restapi.Version, logger.CORESTARTED,
		)

		// Start MongoDB
		{
			var (
				db  mongod.Storer
				err error
			)

			dbName := viper.GetString("db.database")
			dbhost := viper.GetString("db.host")
			dbport := strconv.Itoa(viper.GetInt("db.port"))
			l.Core().DSSDebug(dbhost, dbport, dbName, viper.GetString("db.login"), viper.GetString("db.password"))

			db, err = mongod.NewCli(
				dbhost,
				dbport,
				viper.GetString("db.login"),
				viper.GetString("db.password"),
				dbName,
			)
			if err != nil {
				l.Core().DSSDebug(err)
				l.Core().DSSFatal("db", dbhost, dbport, logger.COREFAILED)
			}
			defer func() {
				err = db.Close()
			}()

			err = db.Connect(context.Background())
			if err != nil {
				l.Core().DSSDebug(err)
				l.Core().DSSFatal("db", dbhost, dbport, logger.COREFAILED)
			}
			l.Core().DSSInfo("db", dbhost, dbport, "", logger.CORECONNECTED)

			if viper.GetBool("status") {
				os.Exit(0)
			}
		}
		// Start JSON API server
		swaggerSpec, err := loads.Embedded(restapi.SwaggerJSON, restapi.FlatSwaggerJSON)
		if err != nil {
			log.Fatalln(err)
		}

		api := operations.NewDSSAPI(swaggerSpec)
		server := restapi.NewServer(api)
		defer func() {
			err = server.Shutdown()
		}()

		// Apply server options
		server.Host = viper.GetString("http.host")
		server.Port = viper.GetInt("http.port")

		server.ConfigureAPI()

		if err := server.Serve(); err != nil {
			log.Fatalln(err)
		}
	},
}

func main() {
	if err := cmd.Execute(); err != nil {
		log.Fatal(err.Error())
	}
}

func init() {
	restapi.Version = version
	cmd.SetVersionTemplate(fmt.Sprintf("Version: %s\n", restapi.Version))

	cobra.OnInitialize(initConfig)

	/*
		Set command flags
	*/
	cmd.PersistentFlags().Bool("status", false, "liveness db probe")

	cmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "Config file path")

	cmd.PersistentFlags().String("http.host", "", "API host")
	cmd.PersistentFlags().Int("http.port", 0, "API port")

	cmd.PersistentFlags().String("db.host", "", "Mongodb host")
	cmd.PersistentFlags().Int("db.port", 0, "Mongodb port")
	cmd.PersistentFlags().String("db.login", "", "Mongodb login")
	cmd.PersistentFlags().String("db.password", "", "Mongodb password")
	cmd.PersistentFlags().String("db.database", "test", "Mongodb db name")

	cmd.PersistentFlags().String("logging.output", "STDOUT", "Logging output")
	cmd.PersistentFlags().String("logging.level", "INFO", "Logging level")
	cmd.PersistentFlags().String("logging.format", "TEXT", "Logging format: TEXT or JSON")

	cmd.PersistentFlags().String("sentryDSN", "", "Sentry DSN")

	// Bind command flags to config variables
	viper.BindPFlags(cmd.PersistentFlags())

	// Set sentry definitions
	dsn := viper.GetString("sentrydsn")
	if dsn != "" {
		raven.SetDSN(dsn)
	}
}

func initConfig() {
	// Use config file from the flag if provided.
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)

		if err := viper.ReadInConfig(); err != nil {
			log.Fatal("Can't read config:", err)
		}
	}

	viper.SetEnvPrefix("DSS")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
}

func setOutput() *os.File {
	if viper.GetString("logging.output") != "STDOUT" {
		logFile, err := os.OpenFile(viper.GetString("logging.output"), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		defer func() {
			err = logFile.Close()
			if err != nil {
				log.Fatalf("error closing file: %v", err)
			}
		}()
		if err != nil {
			log.Fatalf("error opening file: %v", err)
		}
		return logFile

	}
	return os.Stdout
}
