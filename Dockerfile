FROM golang:alpine

RUN apk add --no-cache --update \
	git

# dss app

ENV GOPATH=/go \
	PATH="/go/bin:$PATH"

RUN go get -u github.com/golang/dep/cmd/dep

# Copy the local package files to the container's workspace. Add to GOPATH
ADD . /go/src/repo.nefrosovet.ru/maximus-platform/dss

WORKDIR /go/src/repo.nefrosovet.ru/maximus-platform/dss

# RUN dep ensure

# Build
RUN go install repo.nefrosovet.ru/maximus-platform/dss/cmd/dss

# Run the dss command by default when the container starts.
ENTRYPOINT /go/bin/dss

# Service listens on port 80.
EXPOSE 80
