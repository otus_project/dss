package tests

import (
	"os"
	"testing"

	"github.com/go-openapi/strfmt"
	"github.com/stretchr/testify/suite"

	"gitlab.com/otus_project/dss/api/handlers"
	"gitlab.com/otus_project/dss/api/handlers/tests/inmemory"
	"gitlab.com/otus_project/dss/api/models"
	"gitlab.com/otus_project/dss/api/restapi/operations/symptom"
	"gitlab.com/otus_project/dss/cmd/dss/domain"
	"gitlab.com/otus_project/dss/logger"
)

type SymptomSuite struct {
	suite.Suite

	rr *inmemory.RepositoryHolder

	c *handlers.SymptomController
}

func (s *SymptomSuite) SetupTest() {
	s.rr = inmemory.NewRepositories()

	l := logger.NewLogger(
		os.Stdout,
		"ERROR",
		"",
	)

	s.c = handlers.NewSymptomController(
		s.rr.SymptomRepository,
		l.Api().Symptom(),
	)
}

func (s *SymptomSuite) TestPost() {
	// Prepare

	defaultParams := func() symptom.SymptomCreateParams {
		params := symptom.NewSymptomCreateParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params symptom.SymptomCreateParams, expectedType interface{}) interface{} {
		res := s.c.Post(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *symptom.SymptomCreateBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *symptom.SymptomCreateOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// Post symptom with empty body
		verify(defaultParams(), &symptom.SymptomCreateBadRequest{})
	}

	{
		// Post symptom

		params := defaultParams()
		params.Body = symptom.SymptomCreateBody{
			CodeData: models.CodeData{
				Code: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),
			},
			SymptomObject: models.SymptomObject{
				Description: domain.PtrS("description"),
			},
		}

		payload := verify(params, &symptom.SymptomCreateOK{}).(*symptom.SymptomCreateOKBody)

		s.Equal(params.Body.SymptomObject, payload.Data[0].SymptomObject)
	}

	{
		// Post exist symptom

		params := defaultParams()
		params.Body = symptom.SymptomCreateBody{
			CodeData: models.CodeData{
				Code: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),
			},
			SymptomObject: models.SymptomObject{
				Description: domain.PtrS("description"),
			},
		}

		verify(params, &symptom.SymptomCreateBadRequest{})
	}
}

func (s *SymptomSuite) TestGet() {
	// Prepare

	defaultParams := func() symptom.SymptomViewParams {
		params := symptom.NewSymptomViewParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params symptom.SymptomViewParams, expectedType interface{}) interface{} {
		res := s.c.Get(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *symptom.SymptomViewBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *symptom.SymptomViewNotFound:
			checkResponseServiceFields(s.Assert(), res.Payload.Error404Data)
			return res.Payload
		case *symptom.SymptomViewOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// Get nonexistent symptom

		params := defaultParams()
		params.SymptomCode = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")

		verify(params, &symptom.SymptomViewNotFound{})
	}

	{
		// Get symptom

		p := domain.Symptom{
			Code: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

			Description: domain.PtrS("description"),
		}

		_, err := s.rr.SymptomRepository.Set(p)
		s.NoError(err)

		params := defaultParams()
		params.SymptomCode = p.Code

		payload := verify(params, &symptom.SymptomViewOK{}).(*symptom.SymptomViewOKBody)

		s.Equal(p.Code, payload.Data[0].Code)
	}
}

func (s *SymptomSuite) TestPatch() {
	// Prepare

	defaultParams := func() symptom.SymptomUpdateParams {
		params := symptom.NewSymptomUpdateParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params symptom.SymptomUpdateParams, expectedType interface{}) interface{} {
		res := s.c.Patch(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *symptom.SymptomUpdateBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *symptom.SymptomUpdateNotFound:
			checkResponseServiceFields(s.Assert(), res.Payload.Error404Data)
			return res.Payload
		case *symptom.SymptomUpdateOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// Update nonexistent symptom

		params := defaultParams()
		params.SymptomCode = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")

		params.Body = symptom.SymptomUpdateBody{
			SymptomObject: models.SymptomObject{
				Description: domain.PtrS("description"),
			},
		}

		verify(params, &symptom.SymptomUpdateNotFound{})
	}

	{
		// Update symptom

		params := defaultParams()

		p := domain.Symptom{
			Code: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

			Description: domain.PtrS("description"),
		}

		_, err := s.rr.SymptomRepository.Set(p)
		s.NoError(err)

		params.SymptomCode = p.Code
		params.Body = symptom.SymptomUpdateBody{
			SymptomObject: models.SymptomObject{
				Description: domain.PtrS("another description"),
			},
		}

		payload := verify(params, &symptom.SymptomUpdateOK{}).(*symptom.SymptomUpdateOKBody)

		s.Equal(p.Code, payload.Data[0].Code)
		s.Equal("another description", *payload.Data[0].Description)
	}
}

func (s *SymptomSuite) TestDelete() {
	// Prepare

	defaultParams := func() symptom.SymptomDeleteParams {
		params := symptom.NewSymptomDeleteParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params symptom.SymptomDeleteParams, expectedType interface{}) interface{} {
		res := s.c.Delete(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *symptom.SymptomDeleteBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *symptom.SymptomDeleteNotFound:
			checkResponseServiceFields(s.Assert(), res.Payload.Error404Data)
			return res.Payload
		case *symptom.SymptomDeleteOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// Delete nonexistent symptom

		params := defaultParams()
		params.SymptomCode = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")

		verify(params, &symptom.SymptomDeleteNotFound{})
	}

	{
		// Delete symptom

		params := defaultParams()

		p := domain.Symptom{
			Code: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

			Description: domain.PtrS("description"),
		}

		_, err := s.rr.SymptomRepository.Set(p)
		s.NoError(err)

		params.SymptomCode = p.Code

		verify(params, &symptom.SymptomDeleteOK{})
	}
}
func TestSymptom(t *testing.T) {
	suite.Run(t, new(SymptomSuite))
}
