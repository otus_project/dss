package tests

import (
	"os"
	"testing"
	"time"

	"github.com/go-openapi/strfmt"
	"github.com/stretchr/testify/suite"

	"gitlab.com/otus_project/dss/api/handlers"
	"gitlab.com/otus_project/dss/api/handlers/tests/inmemory"
	"gitlab.com/otus_project/dss/api/models"
	"gitlab.com/otus_project/dss/api/restapi/operations/patient"
	"gitlab.com/otus_project/dss/cmd/dss/domain"
	"gitlab.com/otus_project/dss/logger"
)

type PatientSuite struct {
	suite.Suite

	rr *inmemory.RepositoryHolder

	c *handlers.PatientController
}

func (s *PatientSuite) SetupTest() {
	s.rr = inmemory.NewRepositories()

	log := logger.NewLogger(
		os.Stdout,
		"ERROR",
		"",
	)

	s.c = handlers.NewPatientController(
		s.rr.PatientRepository,
		s.rr.PatientParamsRepository,
		s.rr.RecommendationRepository,
		log,
	)
}

func (s *PatientSuite) TestPost() {
	// Prepare

	defaultParams := func() patient.PatientCreateParams {
		params := patient.NewPatientCreateParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params patient.PatientCreateParams, expectedType interface{}) interface{} {
		res := s.c.Post(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *patient.PatientCreateBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *patient.PatientCreateOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// Post patient with empty body
		verify(defaultParams(), &patient.PatientCreateBadRequest{})
	}

	{
		// Post patient

		params := defaultParams()
		params.Body = patient.PatientCreateBody{
			GUIDData: models.GUIDData{
				GUID: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),
			},
			PatientObject: models.PatientObject{
				BirthDate:  strfmt.Date(time.Now()),
				CkdCode:    "5D",
				GenderCode: "M",
			},
		}

		payload := verify(params, &patient.PatientCreateOK{}).(*patient.PatientCreateOKBody)

		s.Equal(params.Body.PatientObject, payload.Data[0].PatientObject)
	}

	{
		// Post exist patient

		params := defaultParams()
		params.Body = patient.PatientCreateBody{
			GUIDData: models.GUIDData{
				GUID: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),
			},
			PatientObject: models.PatientObject{
				BirthDate:  strfmt.Date(time.Now()),
				CkdCode:    "5D",
				GenderCode: "M",
			},
		}

		verify(params, &patient.PatientCreateBadRequest{})
	}
}

func (s *PatientSuite) TestGet() {
	// Prepare

	defaultParams := func() patient.PatientViewParams {
		params := patient.NewPatientViewParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params patient.PatientViewParams, expectedType interface{}) interface{} {
		res := s.c.Get(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *patient.PatientViewBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *patient.PatientViewNotFound:
			checkResponseServiceFields(s.Assert(), res.Payload.Error404Data)
			return res.Payload
		case *patient.PatientViewOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// Get nonexistent patient

		params := defaultParams()
		params.PatientGUID = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")

		verify(params, &patient.PatientViewNotFound{})
	}

	{
		// Get patient

		p := domain.Patient{
			GUID: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

			BirthDate:  time.Now(),
			CkdCode:    "5D",
			GenderCode: "M",
		}

		_, err := s.rr.PatientRepository.Set(p)
		s.NoError(err)

		params := defaultParams()
		params.PatientGUID = p.GUID

		payload := verify(params, &patient.PatientViewOK{}).(*patient.PatientViewOKBody)

		s.Equal(p.GUID, payload.Data[0].GUID)
	}
}

func (s *PatientSuite) TestPatch() {
	// Prepare

	defaultParams := func() patient.PatientUpdateParams {
		params := patient.NewPatientUpdateParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params patient.PatientUpdateParams, expectedType interface{}) interface{} {
		res := s.c.Patch(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *patient.PatientUpdateBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *patient.PatientUpdateNotFound:
			checkResponseServiceFields(s.Assert(), res.Payload.Error404Data)
			return res.Payload
		case *patient.PatientUpdateOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// Update nonexistent patient

		params := defaultParams()
		params.PatientGUID = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")

		params.Body = patient.PatientUpdateBody{
			PatientObject: models.PatientObject{
				GenderCode: "male",
			},
		}

		verify(params, &patient.PatientUpdateNotFound{})
	}

	{
		// Update patient

		params := defaultParams()

		p := domain.Patient{
			GUID: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

			BirthDate:  time.Now(),
			CkdCode:    "5D",
			GenderCode: "M",
		}

		_, err := s.rr.PatientRepository.Set(p)
		s.NoError(err)

		params.PatientGUID = p.GUID
		params.Body = patient.PatientUpdateBody{
			PatientObject: models.PatientObject{
				GenderCode: "F",
			},
		}

		payload := verify(params, &patient.PatientUpdateOK{}).(*patient.PatientUpdateOKBody)

		s.Equal(p.GUID, payload.Data[0].GUID)
		s.Equal("F", payload.Data[0].GenderCode)
	}
}

func (s *PatientSuite) TestDelete() {
	// Prepare

	defaultParams := func() patient.PatientDeleteParams {
		params := patient.NewPatientDeleteParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params patient.PatientDeleteParams, expectedType interface{}) interface{} {
		res := s.c.Delete(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *patient.PatientDeleteBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *patient.PatientDeleteNotFound:
			checkResponseServiceFields(s.Assert(), res.Payload.Error404Data)
			return res.Payload
		case *patient.PatientDeleteOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// Delete nonexistent patient

		params := defaultParams()
		params.PatientGUID = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")

		verify(params, &patient.PatientDeleteNotFound{})
	}

	{
		// Delete patient

		params := defaultParams()

		p := domain.Patient{
			GUID: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

			BirthDate:  time.Now(),
			CkdCode:    "5D",
			GenderCode: "M",
		}

		_, err := s.rr.PatientRepository.Set(p)
		s.NoError(err)

		params.PatientGUID = p.GUID

		verify(params, &patient.PatientDeleteOK{})
	}
}

func (s *PatientSuite) TestList() {
	// Prepare

	defaultParams := func() patient.PatientCollectionParams {
		params := patient.NewPatientCollectionParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params patient.PatientCollectionParams, expectedType interface{}) interface{} {
		res := s.c.List(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *patient.PatientCollectionBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *patient.PatientCollectionNotFound:
			checkResponseServiceFields(s.Assert(), res.Payload.Error404Data)
			return res.Payload
		case *patient.PatientCollectionOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// Get collection without patients

		params := defaultParams()

		verify(params, &patient.PatientCollectionNotFound{})
	}

	{
		// Get collection

		params := defaultParams()

		p := domain.Patient{
			GUID: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

			BirthDate:  time.Now(),
			CkdCode:    "5D",
			GenderCode: "M",
		}

		_, err := s.rr.PatientRepository.Set(p)
		s.NoError(err)

		params.GenderCode = domain.PtrS("M")

		payload := verify(params, &patient.PatientCollectionOK{}).(*patient.PatientCollectionOKBody)

		s.Equal(p.GenderCode, payload.Data[0].GenderCode)
	}
}

func (s *PatientSuite) TestRecsList() {
	// Prepare

	defaultParams := func() patient.PatientRecommendationViewParams {
		params := patient.NewPatientRecommendationViewParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params patient.PatientRecommendationViewParams, expectedType interface{}) interface{} {
		res := s.c.RecsList(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *patient.PatientRecommendationViewBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *patient.PatientRecommendationViewNotFound:
			checkResponseServiceFields(s.Assert(), res.Payload.Error404Data)
			return res.Payload
		case *patient.PatientRecommendationViewOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		case *patient.PatientViewNotFound:
			checkResponseServiceFields(s.Assert(), res.Payload.Error404Data)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// Get recommendations from nonexistent patient

		params := defaultParams()
		params.PatientGUID = "17013f37-05cd-4eea-a262-c2762c4e494f"

		verify(params, &patient.PatientViewNotFound{})
	}

	p := domain.Patient{
		GUID: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

		BirthDate:  time.Now(),
		CkdCode:    "5D",
		GenderCode: "M",
	}

	_, err := s.rr.PatientRepository.Set(p)
	s.NoError(err)

	{
		// Get patient recommendations

		params := defaultParams()
		params.PatientGUID = "17013f37-05cd-4eea-a262-c2762c4e494f"
		params.GroupCode = domain.PtrS("GC")

		payload := verify(params, &patient.PatientRecommendationViewOK{}).(*patient.PatientRecommendationViewOKBody)

		s.Len(payload.Data, 0)
	}
}

func TestPatient(t *testing.T) {
	suite.Run(t, new(PatientSuite))
}
