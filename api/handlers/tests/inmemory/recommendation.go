package inmemory

import (
	"github.com/emirpasic/gods/maps/linkedhashmap"
	"gitlab.com/otus_project/dss/cmd/dss/domain"
)

type inmemoryRecommendationRepository linkedhashmap.Map

func NewRecommendationRepository() *inmemoryRecommendationRepository {
	return (*inmemoryRecommendationRepository)(linkedhashmap.New())
}

func (p *inmemoryRecommendationRepository) Set(data domain.Recommendation) (*domain.Recommendation, error) {
	t := (*linkedhashmap.Map)(p)

	if _, found := t.Get(data.Code); found {
		return nil, domain.ErrRecommendationAlreadyExists
	}

	t.Put(data.Code, data)
	return &data, nil
}

func (p *inmemoryRecommendationRepository) Get(data domain.Recommendation) (*domain.Recommendation, error) {
	t := (*linkedhashmap.Map)(p)

	geted, found := t.Get(data.Code)
	if !found {
		return nil, domain.ErrRecommendationNotFound
	}

	nd := geted.(domain.Recommendation)
	return &nd, nil
}

func (p *inmemoryRecommendationRepository) Update(oldData domain.Recommendation, newData domain.Recommendation) (*domain.Recommendation, error) {
	t := (*linkedhashmap.Map)(p)

	newData.Code = oldData.Code

	if _, found := t.Get(oldData.Code); !found {
		return nil, domain.ErrRecommendationNotFound
	}
	t.Put(newData.Code, newData)

	return &newData, nil
}

func (p *inmemoryRecommendationRepository) Delete(data domain.Recommendation) error {
	t := (*linkedhashmap.Map)(p)

	if _, found := t.Get(data.Code); !found {
		return domain.ErrRecommendationNotFound
	}

	t.Remove(data.Code)

	return nil
}

func (p *inmemoryRecommendationRepository) List(data domain.RecommendationsFilter) ([]domain.Recommendation, error) {
	t := (*linkedhashmap.Map)(p)

	filtered := t.Select(func(key interface{}, value interface{}) bool {
		recommendation := value.(domain.Recommendation)

		return data.GroupCode == recommendation.GroupCode
	})

	var list []domain.Recommendation
	it := filtered.Iterator()

	for i := int64(0); i < data.Offset; i++ {
		it.Next()
	}

	for it.Next() {
		list = append(list, it.Value().(domain.Recommendation))
		if int64(len(list)) == data.Limit {
			break
		}
	}
	return list, nil
}
