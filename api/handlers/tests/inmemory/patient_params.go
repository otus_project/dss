package inmemory

import (
	"github.com/emirpasic/gods/maps/linkedhashmap"
	"gitlab.com/otus_project/dss/cmd/dss/domain"
)

type inmemoryPatientParamsRepository linkedhashmap.Map

func NewPatientParamsRepository() *inmemoryPatientParamsRepository {
	return (*inmemoryPatientParamsRepository)(linkedhashmap.New())
}

func (p *inmemoryPatientParamsRepository) Set(data domain.PatientParam) (*domain.PatientParam, error) {
	t := (*linkedhashmap.Map)(p)

	if _, found := t.Get(data.GUID); found {
		return nil, domain.ErrPatientDataAlreadyExists
	}

	t.Put(data.GUID, data)
	return &data, nil
}

func (p *inmemoryPatientParamsRepository) Get(data domain.PatientParam) (*domain.PatientParam, error) {
	t := (*linkedhashmap.Map)(p)

	geted, found := t.Get(data.GUID)
	if !found {
		return nil, domain.ErrPatientDataNotFound
	}

	nd := geted.(domain.PatientParam)
	return &nd, nil
}

func (p *inmemoryPatientParamsRepository) Update(oldData domain.PatientParam, newData domain.PatientParam) (*domain.PatientParam, error) {
	t := (*linkedhashmap.Map)(p)

	newData.GUID = oldData.GUID

	if _, found := t.Get(oldData.GUID); !found {
		return nil, domain.ErrPatientDataNotFound
	}

	t.Put(newData.GUID, newData)

	return &newData, nil
}

func (p *inmemoryPatientParamsRepository) Delete(data domain.PatientParam) error {
	t := (*linkedhashmap.Map)(p)
	if _, found := t.Get(data.GUID); !found {
		return domain.ErrPatientDataNotFound
	}

	t.Remove(data.GUID)

	return nil
}

func (p *inmemoryPatientParamsRepository) List(data domain.PatientParamsFilter) ([]domain.PatientParam, error) {
	t := (*linkedhashmap.Map)(p)

	// FIXME: Only for usage in tests
	filtered := t.Select(func(key interface{}, value interface{}) bool {
		patientParams := value.(domain.PatientParam)

		return data.Object.TypeCode == patientParams.TypeCode
	})

	var list []domain.PatientParam
	it := filtered.Iterator()

	for i := int64(0); i < data.Offset; i++ {
		it.Next()
	}

	for it.Next() {
		list = append(list, it.Value().(domain.PatientParam))
		if int64(len(list)) == data.Limit {
			break
		}
	}
	return list, nil
}
