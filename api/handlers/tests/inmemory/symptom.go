package inmemory

import (
	"github.com/emirpasic/gods/maps/linkedhashmap"
	"gitlab.com/otus_project/dss/cmd/dss/domain"
)

type inmemorySymptomRepository linkedhashmap.Map

func NewSymptomRepository() *inmemorySymptomRepository {
	return (*inmemorySymptomRepository)(linkedhashmap.New())
}

func (p *inmemorySymptomRepository) Set(data domain.Symptom) (*domain.Symptom, error) {
	t := (*linkedhashmap.Map)(p)

	if _, found := t.Get(data.Code); found {
		return nil, domain.ErrSymptomAlreadyExists
	}

	t.Put(data.Code, data)
	return &data, nil
}

func (p *inmemorySymptomRepository) Get(data domain.Symptom) (*domain.Symptom, error) {
	t := (*linkedhashmap.Map)(p)

	geted, found := t.Get(data.Code)
	if !found {
		return nil, domain.ErrSymptomNotFound
	}

	nd := geted.(domain.Symptom)
	return &nd, nil
}

func (p *inmemorySymptomRepository) Update(oldData domain.Symptom, newData domain.Symptom) (*domain.Symptom, error) {
	t := (*linkedhashmap.Map)(p)

	newData.Code = oldData.Code

	if _, found := t.Get(oldData.Code); !found {
		return nil, domain.ErrSymptomNotFound
	}
	t.Put(newData.Code, newData)

	return &newData, nil
}

func (p *inmemorySymptomRepository) Delete(data domain.Symptom) error {
	t := (*linkedhashmap.Map)(p)

	if _, found := t.Get(data.Code); !found {
		return domain.ErrSymptomNotFound
	}

	t.Remove(data.Code)

	return nil
}

func (p *inmemorySymptomRepository) List(data domain.SymptomsFilter) ([]domain.Symptom, error) {
	t := (*linkedhashmap.Map)(p)

	var list []domain.Symptom
	it := t.Iterator()

	for i := int64(0); i < data.Offset; i++ {
		it.Next()
	}

	for it.Next() {
		list = append(list, it.Value().(domain.Symptom))
		if int64(len(list)) == data.Limit {
			break
		}
	}
	return list, nil
}
