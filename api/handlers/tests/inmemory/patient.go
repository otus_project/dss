package inmemory

import (
	"github.com/emirpasic/gods/maps/linkedhashmap"
	"gitlab.com/otus_project/dss/cmd/dss/domain"
)

type inmemoryPatientRepository linkedhashmap.Map

func NewPatientRepository() *inmemoryPatientRepository {
	return (*inmemoryPatientRepository)(linkedhashmap.New())
}

func (p *inmemoryPatientRepository) Set(data domain.Patient) (*domain.Patient, error) {
	t := (*linkedhashmap.Map)(p)

	_, found := t.Get(data.GUID)
	if found {
		return nil, domain.ErrPatientAlreadyExists
	}

	t.Put(data.GUID, data)
	return &data, nil
}

func (p *inmemoryPatientRepository) Get(data domain.Patient) (*domain.Patient, error) {
	t := (*linkedhashmap.Map)(p)

	geted, found := t.Get(data.GUID)
	if !found {
		return nil, domain.ErrPatientNotFound
	}

	nd := geted.(domain.Patient)
	return &nd, nil
}

func (p *inmemoryPatientRepository) Update(oldData domain.Patient, newData domain.Patient) (*domain.Patient, error) {
	t := (*linkedhashmap.Map)(p)

	newData.GUID = oldData.GUID

	if _, found := t.Get(oldData.GUID); !found {
		return nil, domain.ErrPatientNotFound
	}
	t.Put(newData.GUID, newData)

	return &newData, nil
}

func (p *inmemoryPatientRepository) Delete(data domain.Patient) error {
	t := (*linkedhashmap.Map)(p)

	if _, found := t.Get(data.GUID); !found {
		return domain.ErrPatientNotFound
	}

	t.Remove(data.GUID)

	return nil
}

func (p *inmemoryPatientRepository) List(data domain.PatientsFilter) ([]domain.Patient, error) {
	t := (*linkedhashmap.Map)(p)

	if t.Size() == 0 {
		return nil, domain.ErrPatientNotFound
	}

	// FIXME: Only for usage in tests
	filtered := t.Select(func(key interface{}, value interface{}) bool {
		patient := value.(domain.Patient)

		return data.Object.GenderCode == patient.GenderCode
	})

	var list []domain.Patient
	it := filtered.Iterator()

	for i := int64(0); i < data.Offset; i++ {
		it.Next()
	}

	for it.Next() {
		list = append(list, it.Value().(domain.Patient))
		if int64(len(list)) == data.Limit {
			break
		}
	}
	return list, nil
}
