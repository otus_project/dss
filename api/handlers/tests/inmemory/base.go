package inmemory

import (
	"gitlab.com/otus_project/dss/cmd/dss/db/mongod"
)

type RepositoryHolder struct {
	PatientRepository        mongod.PatientRepository
	PatientParamsRepository  mongod.PatientParamsRepository
	RecommendationRepository mongod.RecommendationRepository
	SymptomRepository        mongod.SymptomRepository
}

func NewRepositories() *RepositoryHolder {
	return &RepositoryHolder{
		PatientRepository:        NewPatientRepository(),
		PatientParamsRepository:  NewPatientParamsRepository(),
		RecommendationRepository: NewRecommendationRepository(),
		SymptomRepository:        NewSymptomRepository(),
	}
}
