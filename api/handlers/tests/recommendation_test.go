package tests

import (
	"os"
	"testing"

	"github.com/go-openapi/strfmt"
	"github.com/stretchr/testify/suite"

	"gitlab.com/otus_project/dss/api/handlers"
	"gitlab.com/otus_project/dss/api/handlers/tests/inmemory"
	"gitlab.com/otus_project/dss/api/models"
	"gitlab.com/otus_project/dss/api/restapi/operations/recommendation"
	"gitlab.com/otus_project/dss/cmd/dss/domain"
	"gitlab.com/otus_project/dss/logger"
)

type RecommendationSuite struct {
	suite.Suite

	rr *inmemory.RepositoryHolder

	c *handlers.RecommendationController
}

func (s *RecommendationSuite) SetupTest() {
	s.rr = inmemory.NewRepositories()

	l := logger.NewLogger(
		os.Stdout,
		"ERROR",
		"",
	)

	s.c = handlers.NewRecommendationController(
		s.rr.RecommendationRepository,
		l.Api().Recommendation(),
	)
}

func (s *RecommendationSuite) TestPost() {
	// Prepare

	defaultParams := func() recommendation.RecommendationCreateParams {
		params := recommendation.NewRecommendationCreateParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params recommendation.RecommendationCreateParams, expectedType interface{}) interface{} {
		res := s.c.Post(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *recommendation.RecommendationCreateBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *recommendation.RecommendationCreateOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// Post recommendation with empty body
		verify(defaultParams(), &recommendation.RecommendationCreateBadRequest{})
	}

	{
		// Post recommendation

		params := defaultParams()
		params.Body = recommendation.RecommendationCreateBody{
			CodeData: models.CodeData{
				Code: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),
			},
			RecommendationObject: models.RecommendationObject{
				GroupCode:   "GC",
				Description: domain.PtrS("description"),
			},
		}

		payload := verify(params, &recommendation.RecommendationCreateOK{}).(*recommendation.RecommendationCreateOKBody)

		s.Equal(params.Body.RecommendationObject, payload.Data[0].RecommendationObject)
	}

	{
		// Post exist recommendation

		params := defaultParams()
		params.Body = recommendation.RecommendationCreateBody{
			CodeData: models.CodeData{
				Code: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),
			},
			RecommendationObject: models.RecommendationObject{
				GroupCode:   "GC",
				Description: domain.PtrS("description"),
			},
		}

		verify(params, &recommendation.RecommendationCreateBadRequest{})
	}
}

func (s *RecommendationSuite) TestGet() {
	// Prepare

	defaultParams := func() recommendation.RecommendationViewParams {
		params := recommendation.NewRecommendationViewParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params recommendation.RecommendationViewParams, expectedType interface{}) interface{} {
		res := s.c.Get(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *recommendation.RecommendationViewBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *recommendation.RecommendationViewNotFound:
			checkResponseServiceFields(s.Assert(), res.Payload.Error404Data)
			return res.Payload
		case *recommendation.RecommendationViewOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// Get nonexistent recommendation

		params := defaultParams()
		params.RecommendationCode = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")

		verify(params, &recommendation.RecommendationViewNotFound{})
	}

	{
		// Get recommendation

		p := domain.Recommendation{
			Code: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

			GroupCode: "GC",
		}

		_, err := s.rr.RecommendationRepository.Set(p)
		s.NoError(err)

		params := defaultParams()
		params.RecommendationCode = p.Code

		payload := verify(params, &recommendation.RecommendationViewOK{}).(*recommendation.RecommendationViewOKBody)

		s.Equal(p.Code, payload.Data[0].Code)
	}
}

func (s *RecommendationSuite) TestPatch() {
	// Prepare

	defaultParams := func() recommendation.RecommendationUpdateParams {
		params := recommendation.NewRecommendationUpdateParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params recommendation.RecommendationUpdateParams, expectedType interface{}) interface{} {
		res := s.c.Patch(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *recommendation.RecommendationUpdateBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *recommendation.RecommendationUpdateNotFound:
			checkResponseServiceFields(s.Assert(), res.Payload.Error404Data)
			return res.Payload
		case *recommendation.RecommendationUpdateOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// Update nonexistent recommendation

		params := defaultParams()
		params.RecommendationCode = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")

		params.Body = recommendation.RecommendationUpdateBody{
			RecommendationObject: models.RecommendationObject{
				GroupCode: "GC",
			},
		}

		verify(params, &recommendation.RecommendationUpdateNotFound{})
	}

	{
		// Update recommendation

		params := defaultParams()

		p := domain.Recommendation{
			Code: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

			GroupCode: "GC",
		}

		_, err := s.rr.RecommendationRepository.Set(p)
		s.NoError(err)

		params.RecommendationCode = p.Code
		params.Body = recommendation.RecommendationUpdateBody{
			RecommendationObject: models.RecommendationObject{
				Description: domain.PtrS("another description"),
			},
		}

		payload := verify(params, &recommendation.RecommendationUpdateOK{}).(*recommendation.RecommendationUpdateOKBody)

		s.Equal(p.Code, payload.Data[0].Code)
		s.Equal("another description", *payload.Data[0].Description)
	}
}

func (s *RecommendationSuite) TestDelete() {
	// Prepare

	defaultParams := func() recommendation.RecommendationDeleteParams {
		params := recommendation.NewRecommendationDeleteParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params recommendation.RecommendationDeleteParams, expectedType interface{}) interface{} {
		res := s.c.Delete(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *recommendation.RecommendationDeleteBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *recommendation.RecommendationDeleteNotFound:
			checkResponseServiceFields(s.Assert(), res.Payload.Error404Data)
			return res.Payload
		case *recommendation.RecommendationDeleteOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// Delete nonexistent recommendation

		params := defaultParams()
		params.RecommendationCode = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")

		verify(params, &recommendation.RecommendationDeleteNotFound{})
	}

	{
		// Delete recommendation

		params := defaultParams()

		p := domain.Recommendation{
			Code: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

			GroupCode: "GC",
		}

		_, err := s.rr.RecommendationRepository.Set(p)
		s.NoError(err)

		params.RecommendationCode = p.Code

		verify(params, &recommendation.RecommendationDeleteOK{})
	}
}

func TestRecommendation(t *testing.T) {
	suite.Run(t, new(RecommendationSuite))
}
