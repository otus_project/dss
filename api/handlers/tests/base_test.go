package tests

import (
	"net/http"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/otus_project/dss/api/handlers"
	"gitlab.com/otus_project/dss/api/models"
)

func getFakeRequest() *http.Request {
	return &http.Request{
		RequestURI: "/",
	}
}

func checkResponseServiceFields(r *assert.Assertions, payload interface{}) {
	switch p := payload.(type) {
	case models.SuccessData:
		r.Equal(*handlers.PayloadSuccessMessage, *p.Message)
		r.Empty(p.Errors)
		r.Equal(handlers.Version, p.Version)
	case models.Error400Data:
		r.Equal(*handlers.PayloadValidationErrorMessage, *p.Message)
		r.Equal(handlers.Version, p.Version)
	case models.Error404Data:
		r.Equal(*handlers.NotFoundMessage, *p.Message)
		r.Equal(handlers.Version, p.Version)
	case models.Error405Data:
		r.Equal(handlers.Version, p.Version)
	case models.Error500Data:
		r.Equal(*handlers.InternalServerErrorMessage, *p.Message)
		r.Equal(handlers.Version, p.Version)
	default:
		logrus.Fatalf("There is no check of service fields for %T", p)
	}
}
