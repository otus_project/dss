package tests

import (
	"testing"
	"time"

	"github.com/go-openapi/strfmt"
	"github.com/stretchr/testify/suite"

	"gitlab.com/otus_project/dss/api/handlers"
	"gitlab.com/otus_project/dss/api/handlers/tests/inmemory"
	"gitlab.com/otus_project/dss/api/models"
	"gitlab.com/otus_project/dss/api/restapi/operations/patient"
	"gitlab.com/otus_project/dss/cmd/dss/domain"
)

type PatientParamsSuite struct {
	suite.Suite

	rr *inmemory.RepositoryHolder

	c *handlers.PatientParamsController
}

func (s *PatientParamsSuite) SetupTest() {
	s.rr = inmemory.NewRepositories()

	s.c = handlers.NewPatientParamsController(
		s.rr.PatientRepository,
		s.rr.PatientParamsRepository,
	)
}

func (s *PatientParamsSuite) TestPost() {
	// Prepare

	defaultParams := func() patient.PatientDataCreateParams {
		params := patient.NewPatientDataCreateParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params patient.PatientDataCreateParams, expectedType interface{}) interface{} {
		res := s.c.Post(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *patient.PatientDataCreateBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *patient.PatientDataCreateNotFound:
			checkResponseServiceFields(s.Assert(), res.Payload.Error404Data)
			return res.Payload
		case *patient.PatientDataCreateOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// Post patient data with empty body

		verify(defaultParams(), &patient.PatientDataCreateBadRequest{})
	}

	{
		// Post patient data with nonexistent patient guid

		params := defaultParams()
		params.PatientGUID = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")

		params.Body = patient.PatientDataCreateBody{
			PatientParamObject: models.PatientParamObject{
				TypeCode: "TC",
			},
		}

		verify(params, &patient.PatientDataCreateNotFound{})
	}

	p := domain.Patient{
		GUID: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

		BirthDate:  time.Now(),
		CkdCode:    "5D",
		GenderCode: "M",
	}

	_, err := s.rr.PatientRepository.Set(p)
	s.NoError(err)

	{
		// Post patient data

		params := defaultParams()
		params.PatientGUID = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")

		params.Body = patient.PatientDataCreateBody{
			PatientParamObject: models.PatientParamObject{
				TypeCode: "TC",
			},
		}

		verify(params, &patient.PatientDataCreateOK{})
	}
}

func (s *PatientParamsSuite) TestPut() {
	// Prepare

	defaultParams := func() patient.PatientDataUpdateParams {
		params := patient.NewPatientDataUpdateParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params patient.PatientDataUpdateParams, expectedType interface{}) interface{} {
		res := s.c.Put(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *patient.PatientDataUpdateBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *patient.PatientDataUpdateNotFound:
			checkResponseServiceFields(s.Assert(), res.Payload.Error404Data)
			return res.Payload
		case *patient.PatientDataUpdateOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// Put patient data with empty body

		params := defaultParams()

		verify(params, &patient.PatientDataUpdateNotFound{})
	}

	{
		// Put patient data with nonexistent patient guid

		params := defaultParams()
		params.PatientGUID = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")

		params.Body = patient.PatientDataUpdateBody{
			PatientParamObject: models.PatientParamObject{
				TypeCode: "TC",
			},
		}

		verify(params, &patient.PatientDataUpdateNotFound{})
	}

	p := domain.Patient{
		GUID: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

		BirthDate:  time.Now(),
		CkdCode:    "5D",
		GenderCode: "M",
	}

	_, err := s.rr.PatientRepository.Set(p)
	s.NoError(err)

	{
		// Put patient data with nonexistent data

		params := defaultParams()
		params.PatientGUID = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")

		params.Body = patient.PatientDataUpdateBody{
			PatientParamObject: models.PatientParamObject{
				TypeCode: "TC",
			},
		}

		verify(params, &patient.PatientDataUpdateNotFound{})
	}

	pp := domain.PatientParam{
		GUID:        strfmt.UUID("27013f37-05cd-4eea-a262-c2762c4e494f"),
		PatientGUID: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

		TypeCode: "TC",
	}

	_, err = s.rr.PatientParamsRepository.Set(pp)
	s.NoError(err)

	{
		// Put patient data

		params := defaultParams()
		params.PatientGUID = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")
		params.ParamGUID = strfmt.UUID("27013f37-05cd-4eea-a262-c2762c4e494f")

		params.Body = patient.PatientDataUpdateBody{
			PatientParamObject: models.PatientParamObject{
				TypeCode: "TC",
			},
		}

		verify(params, &patient.PatientDataUpdateOK{})
	}
}

func (s *PatientParamsSuite) TestDelete() {
	// Prepare

	defaultParams := func() patient.PatientDataDeleteParams {
		params := patient.NewPatientDataDeleteParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params patient.PatientDataDeleteParams, expectedType interface{}) interface{} {
		res := s.c.Delete(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *patient.PatientDataDeleteBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *patient.PatientDataDeleteNotFound:
			checkResponseServiceFields(s.Assert(), res.Payload.Error404Data)
			return res.Payload
		case *patient.PatientDataDeleteOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// Delete nonexistent patient data

		verify(defaultParams(), &patient.PatientDataDeleteNotFound{})
	}

	{
		// Delete patient data with nonexistent patient

		params := defaultParams()
		params.PatientGUID = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")

		verify(params, &patient.PatientDataDeleteNotFound{})
	}

	p := domain.Patient{
		GUID: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

		BirthDate:  time.Now(),
		CkdCode:    "5D",
		GenderCode: "M",
	}

	_, err := s.rr.PatientRepository.Set(p)
	s.NoError(err)

	pp := domain.PatientParam{
		GUID:        strfmt.UUID("27013f37-05cd-4eea-a262-c2762c4e494f"),
		PatientGUID: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

		TypeCode: "TC",
	}

	_, err = s.rr.PatientParamsRepository.Set(pp)
	s.NoError(err)

	{
		// Delete patient data

		params := defaultParams()
		params.PatientGUID = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")
		params.ParamGUID = strfmt.UUID("27013f37-05cd-4eea-a262-c2762c4e494f")

		verify(params, &patient.PatientDataDeleteOK{})
	}
}

func (s *PatientParamsSuite) TestList() {
	// Prepare

	defaultParams := func() patient.PatientDataCollectionParams {
		params := patient.NewPatientDataCollectionParams()
		params.HTTPRequest = getFakeRequest()

		return params
	}

	verify := func(params patient.PatientDataCollectionParams, expectedType interface{}) interface{} {
		res := s.c.List(params)
		s.IsType(expectedType, res)

		switch res := res.(type) {
		case *patient.PatientDataCollectionBadRequest:
			checkResponseServiceFields(s.Assert(), res.Payload.Error400Data)
			return res.Payload
		case *patient.PatientDataCollectionNotFound:
			checkResponseServiceFields(s.Assert(), res.Payload.Error404Data)
			return res.Payload
		case *patient.PatientDataCollectionOK:
			checkResponseServiceFields(s.Assert(), res.Payload.SuccessData)
			return res.Payload
		default:
			s.T().Errorf("Unknown response type: %T", res)
		}

		return nil
	}

	// Test

	{
		// List patient data with empty repository

		params := defaultParams()

		verify(params, &patient.PatientDataCollectionNotFound{})
	}

	{
		// List patient data with nonexistent patient

		params := defaultParams()
		params.PatientGUID = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")

		verify(params, &patient.PatientDataCollectionNotFound{})
	}

	p := domain.Patient{
		GUID: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

		BirthDate:  time.Now(),
		CkdCode:    "5D",
		GenderCode: "M",
	}

	_, err := s.rr.PatientRepository.Set(p)
	s.NoError(err)

	{
		// List patient data without params

		params := defaultParams()
		params.PatientGUID = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")

		payload := verify(params, &patient.PatientDataCollectionOK{}).(*patient.PatientDataCollectionOKBody)

		s.Len(payload.Data, 0)
	}

	pp := domain.PatientParam{
		GUID:        strfmt.UUID("27013f37-05cd-4eea-a262-c2762c4e494f"),
		PatientGUID: strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f"),

		TypeCode: "TC",
	}

	_, err = s.rr.PatientParamsRepository.Set(pp)
	s.NoError(err)

	{
		// List patient data

		params := defaultParams()
		params.PatientGUID = strfmt.UUID("17013f37-05cd-4eea-a262-c2762c4e494f")

		params.TypeCode = domain.PtrS("TC")

		payload := verify(params, &patient.PatientDataCollectionOK{}).(*patient.PatientDataCollectionOKBody)

		s.Len(payload.Data, 1)
	}
}

func TestPatientParams(t *testing.T) {
	suite.Run(t, new(PatientParamsSuite))
}
