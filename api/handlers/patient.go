package handlers

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/diegoholiveira/jsonlogic"
	errs "github.com/go-openapi/errors"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"

	"gitlab.com/otus_project/dss/api/models"
	"gitlab.com/otus_project/dss/api/restapi/operations/patient"
	"gitlab.com/otus_project/dss/cmd/dss/db/mongod"
	"gitlab.com/otus_project/dss/cmd/dss/domain"
	"gitlab.com/otus_project/dss/logger"
)

type PatientController struct {
	patRepo   mongod.PatientRepository
	paramRepo mongod.PatientParamsRepository
	recRepo   mongod.RecommendationRepository
	patLog    logger.APILogger
	queryLog  logger.QueryEntrier
}

// NewPatientController is event controller constructor.
func NewPatientController(
	pRepo mongod.PatientRepository,
	ppRepo mongod.PatientParamsRepository,
	rRepo mongod.RecommendationRepository,
	l logger.Logger) *PatientController {
	return &PatientController{
		patRepo:   pRepo,
		paramRepo: ppRepo,
		recRepo:   rRepo,
		patLog:    l.Api().Patient(),
		queryLog:  l.Query(),
	}
}

func (pc *PatientController) Post(params patient.PatientCreateParams) (m middleware.Responder) {
	var err error
	var pat *domain.Patient

	if params.Body.PatientObject == (models.PatientObject{}) {
		payload := new(patient.PatientCreateBadRequestBody)
		err := errs.Required("body", "body")
		compErr := errs.CompositeValidationError(err)
		payload.Errors = New400Errors(compErr, params.HTTPRequest)
		payload.Version = Version
		payload.Message = PayloadValidationErrorMessage
		return patient.NewPatientCreateBadRequest().WithPayload(payload)
	}

	pat, err = pc.patRepo.Set(domain.Patient{
		GUID:       params.Body.GUID,
		BirthDate:  time.Time(params.Body.BirthDate),
		CkdCode:    params.Body.CkdCode,
		GenderCode: params.Body.GenderCode,
	})

	if err == domain.ErrPatientAlreadyExists {
		payload := new(patient.PatientCreateBadRequestBody)
		err := errs.DuplicateItems(params.Body.GUID.String(), params.HTTPRequest.RequestURI)
		compErr := errs.CompositeValidationError(err)
		payload.Errors = New400Errors(compErr, params.HTTPRequest)
		payload.Version = Version
		payload.Message = PayloadValidationErrorMessage
		return patient.NewPatientCreateBadRequest().WithPayload(payload)
	}

	if err != nil {
		pc.patLog.DSSDebug(err)
		payload := new(patient.PatientCreateInternalServerErrorBody)
		payload.Errors = err.Error()
		payload.Version = Version
		payload.Message = PayloadFailMessage
		return patient.NewPatientCreateInternalServerError().WithPayload(payload)
	}

	var payload = new(patient.PatientCreateOKBody)
	item := patientToResponse(pat)

	payload.Version = Version
	payload.Message = PayloadSuccessMessage
	payload.Data = append(payload.Data, item)

	pc.patLog.DSSInfo(pat.GUID.String(), logger.APICREATED)

	return patient.NewPatientCreateOK().WithPayload(payload)
}

func (pc *PatientController) Get(params patient.PatientViewParams) middleware.Responder {
	var err error

	var pat *domain.Patient
	pat, err = pc.patRepo.Get(domain.Patient{
		GUID: params.PatientGUID,
	})

	if err == domain.ErrPatientNotFound {
		return patientViewNotFoundResp(err)
	}

	if err != nil {
		pc.patLog.DSSDebug(err)
		return patientViewInternalServerErrorResp(err)
	}

	var payload = new(patient.PatientViewOKBody)
	item := patientToResponse(pat)

	payload.Version = Version
	payload.Message = PayloadSuccessMessage
	payload.Data = append(payload.Data, item)

	return patient.NewPatientViewOK().WithPayload(payload)
}

func (pc *PatientController) Patch(params patient.PatientUpdateParams) middleware.Responder {
	var err error
	var pat *domain.Patient

	pat, err = pc.patRepo.Update(
		domain.Patient{
			GUID: params.PatientGUID,
		},
		domain.Patient{
			BirthDate:  time.Time(params.Body.BirthDate),
			CkdCode:    params.Body.CkdCode,
			GenderCode: params.Body.GenderCode,
		},
	)

	if err == domain.ErrPatientNotFound {
		payload := new(patient.PatientUpdateNotFoundBody)
		payload.Version = Version
		payload.Message = NotFoundMessage
		return patient.NewPatientUpdateNotFound().WithPayload(payload)
	}

	if err != nil {
		pc.patLog.DSSDebug(err)
		payload := new(patient.PatientUpdateInternalServerErrorBody)
		payload.Errors = err.Error()
		payload.Version = Version
		payload.Message = PayloadFailMessage
		return patient.NewPatientUpdateInternalServerError().WithPayload(payload)
	}

	payload := new(patient.PatientUpdateOKBody)
	item := patientToResponse(pat)

	payload.Version = Version
	payload.Message = PayloadSuccessMessage
	payload.Data = append(payload.Data, item)

	pc.patLog.DSSInfo(pat.GUID.String(), logger.APIUPDATED)

	return patient.NewPatientUpdateOK().WithPayload(payload)
}

func (pc *PatientController) Delete(params patient.PatientDeleteParams) middleware.Responder {
	var err error

	_, err = pc.patRepo.Get(domain.Patient{ // fixing bug with empty deleting result
		GUID: params.PatientGUID,
	})
	if err == domain.ErrPatientNotFound {
		payload := new(patient.PatientDeleteNotFoundBody)
		payload.Version = Version
		payload.Message = NotFoundMessage
		return patient.NewPatientDeleteNotFound().WithPayload(payload)
	}

	err = pc.patRepo.Delete(domain.Patient{
		GUID: params.PatientGUID,
	})
	if err != nil {
		pc.patLog.DSSDebug(err)
		payload := new(patient.PatientDeleteInternalServerErrorBody)
		payload.Errors = err.Error()
		payload.Version = Version
		payload.Message = PayloadFailMessage
		return patient.NewPatientDeleteInternalServerError().WithPayload(payload)
	}

	payload := new(patient.PatientDeleteOKBody)
	payload.Version = Version
	payload.Data = []interface{}{}
	payload.Message = PayloadSuccessMessage

	pc.patLog.DSSInfo(params.PatientGUID.String(), logger.APIDELETED)

	return patient.NewPatientDeleteOK().WithPayload(payload)
}

func (pc *PatientController) List(params patient.PatientCollectionParams) middleware.Responder {
	var err error
	var limit int64
	var offset int64
	var bDate time.Time
	var ckdCode string
	var gender string

	if params.Limit != nil {
		limit = *params.Limit
	}
	if params.Offset != nil {
		offset = *params.Offset
	}
	if params.BirthDate != nil {
		bDate = time.Time(*params.BirthDate)
	}
	if params.CkdCode != nil {
		ckdCode = *params.CkdCode
	}
	if params.GenderCode != nil {
		gender = *params.GenderCode
	}

	var pats []domain.Patient
	pats, err = pc.patRepo.List(domain.PatientsFilter{
		Limit:  limit,
		Offset: offset,
		Object: domain.Patient{
			BirthDate:  bDate,
			CkdCode:    ckdCode,
			GenderCode: gender,
		},
	},
	)
	if err == domain.ErrPatientNotFound {
		payload := new(patient.PatientCollectionNotFoundBody)
		payload.Errors = err.Error()
		payload.Version = Version
		payload.Message = NotFoundMessage
		return patient.NewPatientCollectionNotFound().WithPayload(payload)
	}

	if err != nil {
		pc.patLog.DSSDebug(err)
		payload := new(patient.PatientCollectionInternalServerErrorBody)
		payload.Errors = err.Error()
		payload.Version = Version
		payload.Message = PayloadFailMessage
		return patient.NewPatientCollectionInternalServerError().WithPayload(payload)
	}
	var payload = new(patient.PatientCollectionOKBody)

	for _, p := range pats {
		item := patientToResponse(&p)
		payload.Data = append(payload.Data, item)
	}

	payload.Version = Version
	payload.Message = PayloadSuccessMessage

	return patient.NewPatientCollectionOK().WithPayload(payload)
}

func (pc *PatientController) RecsList(params patient.PatientRecommendationViewParams) (resp middleware.Responder) {
	var (
		err       error
		payload   = new(patient.PatientRecommendationViewOKBody)
		groupCode string
	)

	if params.GroupCode != nil {
		groupCode = *params.GroupCode
	}

	// get Patient ==============================================
	var pat *domain.Patient
	pat, err = pc.patRepo.Get(domain.Patient{
		GUID: params.PatientGUID,
	})

	if err == domain.ErrPatientNotFound {
		return patientViewNotFoundResp(err)
	}

	if err != nil {
		pc.patLog.DSSDebug(err)
		return patientViewInternalServerErrorResp(err)
	}

	// get Data of Patient =======================================
	var patDatas []domain.PatientParam
	patDatas, err = pc.paramRepo.List(
		domain.PatientParamsFilter{
			Object: domain.PatientParam{
				PatientGUID: params.PatientGUID,
			},
		},
	)

	if err == domain.ErrPatientDataNotFound {
		patDatas = append(patDatas, domain.PatientParam{})
	}

	if err != nil && err != domain.ErrPatientDataNotFound {
		return patientDataCollectionInternalServerErrorResp(err)
	}

	// get ALL recommendations =====================================
	var recs []domain.Recommendation
	recs, err = pc.recRepo.List(
		domain.RecommendationsFilter{
			GroupCode: groupCode,
		},
	)

	if err == domain.ErrRecommendationNotFound {
		return recommendationCollectionNotFoundResp(err)
	}

	if err != nil {
		pc.patLog.DSSDebug(err)
		return recommendationCollectionInternalServerErrorResp(err)
	}

	for _, pd := range patDatas {
		var btData []byte
		btData, err = json.Marshal(domain.MatchData{
			Patient: struct {
				GenderCode string                 `json:"genderCode"`
				BirthDate  string                 `json:"birthDate"`
				CkdCode    string                 `json:"ckdCode"`
				Params     domain.MatchDataParams `json:"params"`
			}{
				GenderCode: pat.GenderCode,
				BirthDate:  pat.BirthDate.String(),
				CkdCode:    pat.CkdCode,
				Params: domain.MatchDataParams{
					TypeCode: pd.TypeCode,
					Data:     pd.Data,
					Created:  pd.Created,
					Updated:  pd.Updated,
				},
			},
		})
		if err != nil {
			payload.Errors = append(payload.Errors, errors.Wrap(err, pd.GUID.String()))
			pc.patLog.DSSDebug(err)
			continue
		}

		var matchData interface{}
		err = json.Unmarshal(btData, &matchData)
		if err != nil {
			payload.Errors = append(payload.Errors, errors.Wrap(err, pd.GUID.String()))
			pc.patLog.DSSDebug(err)
			continue
		}

		// actual matching
		for _, r := range recs {
			pc.queryLog.DSSInfo(params.PatientGUID.String(), groupCode, r.Code.String())

			btRule, err := bson.MarshalExtJSON(r.Condition, false, false)
			if err != nil {
				payload.Errors = append(payload.Errors, errors.Wrap(err, r.Code.String()))
				continue
			}

			var rule interface{}
			err = json.Unmarshal(btRule, &rule)
			if err != nil {
				payload.Errors = append(payload.Errors, errors.Wrap(err, r.Code.String()))
				pc.patLog.DSSDebug(err)
				continue
			}
			if !jsonlogic.IsValid(rule) {
				payload.Errors = append(
					payload.Errors, errors.Wrap(domain.ErrInvalidConditionRule, r.Code.String()),
				)
				pc.patLog.DSSDebug(domain.ErrInvalidConditionRule)
				continue
			}

			err = doJSONLogic(rule, matchData) // here's the magic
			if err != nil {
				payload.Errors = append(payload.Errors, errors.Wrap(err, r.Code.String()))
				pc.patLog.DSSDebug(err)
				continue
			}

			code := r.Code.String()
			date := strfmt.DateTime(time.Now())

			item := new(patient.PatientRecommendationItem)
			item.Code = &code
			item.Date = &date
			item.Criteria = &models.PatientRecommendationObjectCriteria{
				BirthDate:  strfmt.Date(pat.BirthDate),
				CkdCode:    pat.CkdCode,
				GenderCode: pat.GenderCode,
			}
			if pd.GUID.String() != "" {
				item.Criteria.Params = append(item.Criteria.Params, pd.GUID.String())
			}

			payload.Data = append(payload.Data, item)
		}
	}

	payload.Version = Version
	payload.Message = PayloadSuccessMessage

	return patient.NewPatientRecommendationViewOK().WithPayload(payload)
}

func doJSONLogic(rule interface{}, data interface{}) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = errors.New(fmt.Sprint(r))
		}
	}()
	var (
		result interface{}
		ok     bool
		match  bool
	)

	err = jsonlogic.Apply(rule, data, &result)
	if err != nil {
		return err
	}

	// result must have only bool type
	match, ok = result.(bool)
	if !ok {
		return domain.ErrInvalidResultType
	}
	if !match {
		return domain.ErrParamsRuleMismatch
	}
	return nil
}

func patientViewInternalServerErrorResp(err error) *patient.PatientViewInternalServerError {
	payload := new(patient.PatientViewInternalServerErrorBody)
	payload.Errors = err.Error()
	payload.Version = Version
	payload.Message = PayloadFailMessage
	return patient.NewPatientViewInternalServerError().WithPayload(payload)
}

func patientViewNotFoundResp(err error) *patient.PatientViewNotFound {
	payload := new(patient.PatientViewNotFoundBody)
	payload.Version = Version
	payload.Message = NotFoundMessage
	return patient.NewPatientViewNotFound().WithPayload(payload)

}

func patientToResponse(p *domain.Patient) *patient.DataItems0 {
	created := strfmt.DateTime(p.Created)
	var updated strfmt.DateTime
	if p.Updated != nil {
		updated = strfmt.DateTime(*p.Updated)
	}
	item := new(patient.DataItems0)
	item.GUID = p.GUID
	item.BirthDate = strfmt.Date(p.BirthDate)
	item.GenderCode = p.GenderCode
	item.CkdCode = p.CkdCode
	item.Created = &created
	item.Updated = &updated
	return item
}
