package handlers

import (
	errs "github.com/go-openapi/errors"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"

	"gitlab.com/otus_project/dss/api/models"
	"gitlab.com/otus_project/dss/api/restapi/operations/symptom"
	"gitlab.com/otus_project/dss/cmd/dss/db/mongod"
	"gitlab.com/otus_project/dss/cmd/dss/domain"
	"gitlab.com/otus_project/dss/logger"
)

type SymptomController struct {
	symRepo mongod.SymptomRepository
	l       logger.APILogger
}

// NewSymptomController is event controller constructor.
func NewSymptomController(
	sr mongod.SymptomRepository,
	l logger.APILogger,
) *SymptomController {
	return &SymptomController{
		symRepo: sr,
		l:       l,
	}
}

func (sc *SymptomController) Post(params symptom.SymptomCreateParams) middleware.Responder {
	var err error
	var sym *domain.Symptom

	if params.Body.SymptomObject == (models.SymptomObject{}) {
		payload := new(symptom.SymptomCreateBadRequestBody)
		err := errs.Required("body", "body")
		compErr := errs.CompositeValidationError(err)
		payload.Errors = New400Errors(compErr, params.HTTPRequest)
		payload.Version = Version
		payload.Message = PayloadValidationErrorMessage
		return symptom.NewSymptomCreateBadRequest().WithPayload(payload)
	}

	sym, err = sc.symRepo.Set(domain.Symptom{
		Code:        params.Body.Code,
		Condition:   params.Body.Condition,
		Description: params.Body.Description,
	})
	if err == domain.ErrSymptomAlreadyExists {
		payload := new(symptom.SymptomCreateBadRequestBody)
		err := errs.DuplicateItems(params.Body.Code.String(), params.HTTPRequest.RequestURI)
		compErr := errs.CompositeValidationError(err)
		payload.Errors = New400Errors(compErr, params.HTTPRequest)
		payload.Version = Version
		payload.Message = PayloadValidationErrorMessage
		return symptom.NewSymptomCreateBadRequest().WithPayload(payload)
	}

	if err != nil {
		sc.l.DSSDebug(err)
		payload := new(symptom.SymptomCreateInternalServerErrorBody)
		payload.Errors = err.Error()
		payload.Version = Version
		payload.Message = PayloadFailMessage
		return symptom.NewSymptomCreateInternalServerError().WithPayload(payload)
	}

	var payload = new(symptom.SymptomCreateOKBody)
	item := symptomToResponse(sym)

	payload.Version = Version
	payload.Message = PayloadSuccessMessage
	payload.Data = append(payload.Data, item)

	sc.l.DSSInfo(sym.Code.String(), logger.APICREATED)

	return symptom.NewSymptomCreateOK().WithPayload(payload)
}

func (sc *SymptomController) Get(params symptom.SymptomViewParams) middleware.Responder {
	var err error

	var sym *domain.Symptom
	sym, err = sc.symRepo.Get(domain.Symptom{
		Code: params.SymptomCode,
	})
	if err == domain.ErrSymptomNotFound {
		payload := new(symptom.SymptomViewNotFoundBody)
		payload.Version = Version
		payload.Message = NotFoundMessage
		return symptom.NewSymptomViewNotFound().WithPayload(payload)
	}

	if err != nil {
		sc.l.DSSDebug(err)
		payload := new(symptom.SymptomViewInternalServerErrorBody)
		payload.Errors = err.Error()
		payload.Version = Version
		payload.Message = NotFoundMessage
		return symptom.NewSymptomViewInternalServerError().WithPayload(payload)
	}

	var payload = new(symptom.SymptomViewOKBody)
	item := symptomToResponse(sym)

	payload.Version = Version
	payload.Message = PayloadSuccessMessage
	payload.Data = append(payload.Data, item)

	return symptom.NewSymptomViewOK().WithPayload(payload)
}

func (sc *SymptomController) Patch(params symptom.SymptomUpdateParams) middleware.Responder {
	var err error
	var sym *domain.Symptom

	sym, err = sc.symRepo.Update(
		domain.Symptom{
			Code: params.SymptomCode,
		},
		domain.Symptom{
			Condition:   params.Body.Condition,
			Description: params.Body.Description,
		},
	)
	if err == domain.ErrSymptomNotFound {
		payload := new(symptom.SymptomUpdateNotFoundBody)
		payload.Version = Version
		payload.Message = NotFoundMessage
		return symptom.NewSymptomUpdateNotFound().WithPayload(payload)
	}

	if err != nil {
		sc.l.DSSDebug(err)
		payload := new(symptom.SymptomUpdateInternalServerErrorBody)
		payload.Errors = err.Error()
		payload.Version = Version
		payload.Message = PayloadFailMessage
		return symptom.NewSymptomUpdateInternalServerError().WithPayload(payload)
	}

	var payload = new(symptom.SymptomUpdateOKBody)
	item := symptomToResponse(sym)

	payload.Version = Version
	payload.Message = PayloadSuccessMessage
	payload.Data = append(payload.Data, item)

	sc.l.DSSInfo(sym.Code.String(), logger.APIUPDATED)

	return symptom.NewSymptomUpdateOK().WithPayload(payload)
}

func (sc *SymptomController) Delete(params symptom.SymptomDeleteParams) middleware.Responder {
	var err error

	_, err = sc.symRepo.Get(domain.Symptom{ // fixing bug with empty deleting result
		Code: params.SymptomCode,
	})
	if err == domain.ErrSymptomNotFound {
		payload := new(symptom.SymptomDeleteNotFoundBody)
		payload.Version = Version
		payload.Message = NotFoundMessage
		return symptom.NewSymptomDeleteNotFound().WithPayload(payload)
	}

	err = sc.symRepo.Delete(domain.Symptom{
		Code: params.SymptomCode,
	})
	if err != nil {
		sc.l.DSSDebug(err)
		payload := new(symptom.SymptomDeleteInternalServerErrorBody)
		payload.Errors = err.Error()
		payload.Version = Version
		payload.Message = PayloadFailMessage
		return symptom.NewSymptomDeleteInternalServerError().WithPayload(payload)
	}

	sc.l.DSSInfo(params.SymptomCode.String(), logger.APIDELETED)

	var payload = new(symptom.SymptomDeleteOKBody)
	payload.Version = Version
	payload.Data = []interface{}{}
	payload.Message = PayloadSuccessMessage

	return symptom.NewSymptomDeleteOK().WithPayload(payload)

}

func (sc *SymptomController) List(params symptom.SymptomCollectionParams) middleware.Responder {
	var err error
	var limit int64
	var offset int64

	if params.Limit != nil {
		limit = *params.Limit
	}
	if params.Offset != nil {
		offset = *params.Offset
	}

	var symptoms []domain.Symptom
	symptoms, err = sc.symRepo.List(domain.SymptomsFilter{
		Limit:  limit,
		Offset: offset,
	},
	)
	if err == domain.ErrSymptomNotFound {
		payload := new(symptom.SymptomCollectionNotFoundBody)
		payload.Version = Version
		payload.Message = NotFoundMessage
		return symptom.NewSymptomCollectionNotFound().WithPayload(payload)
	}

	if err != nil {
		sc.l.DSSDebug(err)
		payload := new(symptom.SymptomCollectionInternalServerErrorBody)
		payload.Errors = err.Error()
		payload.Version = Version
		payload.Message = PayloadFailMessage
		return symptom.NewSymptomCollectionInternalServerError().WithPayload(payload)
	}

	var payload = new(symptom.SymptomCollectionOKBody)
	for _, s := range symptoms {
		item := symptomToResponse(&s)
		payload.Data = append(payload.Data, item)
	}

	payload.Version = Version
	payload.Message = PayloadSuccessMessage

	return symptom.NewSymptomCollectionOK().WithPayload(payload)
}

func symptomToResponse(s *domain.Symptom) *symptom.DataItems0 {
	created := strfmt.DateTime(s.Created)
	var updated strfmt.DateTime
	if s.Updated != nil {
		updated = strfmt.DateTime(*s.Updated)
	}
	item := new(symptom.DataItems0)

	item.Code = s.Code
	item.Condition = s.Condition
	item.Description = s.Description
	item.Created = &created
	item.Updated = &updated
	return item
}
