package handlers

import (
	errs "github.com/go-openapi/errors"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"
	"go.mongodb.org/mongo-driver/mongo"
	"gitlab.com/otus_project/dss/api/models"
	"gitlab.com/otus_project/dss/api/restapi/operations/patient"
	"gitlab.com/otus_project/dss/cmd/dss/db/mongod"
	"gitlab.com/otus_project/dss/cmd/dss/domain"
)

type PatientParamsController struct {
	patRepo   mongod.PatientRepository
	paramRepo mongod.PatientParamsRepository
}

// NewPatientDataController is event controller constructor.
func NewPatientParamsController(
	pr mongod.PatientRepository,
	pdr mongod.PatientParamsRepository,
) *PatientParamsController {
	return &PatientParamsController{
		patRepo:   pr,
		paramRepo: pdr,
	}
}

func (pdc *PatientParamsController) Post(params patient.PatientDataCreateParams) middleware.Responder {
	var err error

	if params.Body.PatientParamObject == (models.PatientParamObject{}) {
		payload := new(patient.PatientDataCreateBadRequestBody)
		err := errs.Required("body", "body")
		compErr := errs.CompositeValidationError(err)
		payload.Errors = New400Errors(compErr, params.HTTPRequest)
		payload.Version = Version
		payload.Message = PayloadValidationErrorMessage
		return patient.NewPatientDataCreateBadRequest().WithPayload(payload)
	}

	_, err = pdc.patRepo.Get(domain.Patient{GUID: params.PatientGUID})
	if err == domain.ErrPatientNotFound {
		payload := new(patient.PatientDataCreateNotFoundBody)
		payload.Version = Version
		payload.Message = NotFoundMessage
		return patient.NewPatientDataCreateNotFound().WithPayload(payload)
	}

	var patData *domain.PatientParam
	patData, err = pdc.paramRepo.Set(domain.PatientParam{
		PatientGUID: params.PatientGUID,
		TypeCode:    params.Body.TypeCode,
		Data:        params.Body.Data,
	})
	if err == domain.ErrPatientDataAlreadyExists {
		payload := new(patient.PatientDataCreateBadRequestBody)
		err := errs.DuplicateItems(params.PatientGUID.String(), params.HTTPRequest.RequestURI)
		compErr := errs.CompositeValidationError(err)
		payload.Errors = New400Errors(compErr, params.HTTPRequest)
		payload.Version = Version
		payload.Message = PayloadValidationErrorMessage
		return patient.NewPatientDataCreateBadRequest().WithPayload(payload)
	}
	if err != nil {
		payload := new(patient.PatientDataCreateInternalServerErrorBody)
		payload.Errors = err.Error()
		payload.Version = Version
		payload.Message = PayloadFailMessage
		return patient.NewPatientDataCreateInternalServerError().WithPayload(payload)
	}

	var payload = new(patient.PatientDataCreateOKBody)
	item := patDataToResponse(patData)

	payload.Version = Version
	payload.Message = PayloadSuccessMessage
	payload.Data = append(payload.Data, item)

	return patient.NewPatientDataCreateOK().WithPayload(payload)
}

func (pdc *PatientParamsController) Put(params patient.PatientDataUpdateParams) middleware.Responder {
	var err error
	var patData *domain.PatientParam

	_, err = pdc.patRepo.Get(domain.Patient{
		GUID: params.PatientGUID,
	})
	if err == domain.ErrPatientNotFound {
		payload := new(patient.PatientDataUpdateNotFoundBody)
		payload.Version = Version
		payload.Message = NotFoundMessage
		return patient.NewPatientDataUpdateNotFound().WithPayload(payload)
	}

	patData, err = pdc.paramRepo.Update(
		domain.PatientParam{
			GUID: params.ParamGUID,
		},
		domain.PatientParam{
			TypeCode: params.Body.TypeCode,
			Data:     params.Body.Data,
		},
	)
	if err == domain.ErrPatientDataNotFound {
		payload := new(patient.PatientDataUpdateNotFoundBody)
		payload.Version = Version
		payload.Message = NotFoundMessage
		return patient.NewPatientDataUpdateNotFound().WithPayload(payload)
	}

	if err != nil {
		payload := new(patient.PatientDataUpdateInternalServerErrorBody)
		payload.Errors = err.Error()
		payload.Version = Version
		payload.Message = PayloadFailMessage
		return patient.NewPatientDataUpdateInternalServerError().WithPayload(payload)
	}

	var payload = new(patient.PatientDataUpdateOKBody)
	item := patDataToResponse(patData)

	payload.Version = Version
	payload.Message = PayloadSuccessMessage
	payload.Data = append(payload.Data, item)

	return patient.NewPatientDataUpdateOK().WithPayload(payload)
}

func (pdc *PatientParamsController) Delete(params patient.PatientDataDeleteParams) middleware.Responder {
	var err error

	_, noPatErr := pdc.patRepo.Get(domain.Patient{
		GUID: params.PatientGUID,
	})
	_, noPatDataErr := pdc.paramRepo.Get(domain.PatientParam{ // fixing bug with empty deleting result
		GUID: params.ParamGUID,
	})
	if noPatErr == domain.ErrPatientNotFound || noPatDataErr == domain.ErrPatientDataNotFound {
		payload := new(patient.PatientDataDeleteNotFoundBody)
		payload.Version = Version
		payload.Message = NotFoundMessage
		return patient.NewPatientDataDeleteNotFound().WithPayload(payload)
	}

	err = pdc.paramRepo.Delete(domain.PatientParam{
		GUID: params.ParamGUID,
	})
	if err == mongo.ErrNoDocuments {
		payload := new(patient.PatientDataDeleteNotFoundBody)
		payload.Version = Version
		payload.Message = NotFoundMessage
		return patient.NewPatientDataDeleteNotFound().WithPayload(payload)
	}

	if err != nil {
		payload := new(patient.PatientDataDeleteInternalServerErrorBody)
		payload.Errors = err.Error()
		payload.Version = Version
		payload.Message = PayloadFailMessage
		return patient.NewPatientDataDeleteInternalServerError().WithPayload(payload)
	}

	var payload = new(patient.PatientDataDeleteOKBody)
	payload.Version = Version
	payload.Data = []interface{}{}
	payload.Message = PayloadSuccessMessage

	return patient.NewPatientDataDeleteOK().WithPayload(payload)

}

func (pdc *PatientParamsController) List(params patient.PatientDataCollectionParams) middleware.Responder {
	var err error
	var limit int64
	var offset int64
	var typeCode string

	if params.Limit != nil {
		limit = *params.Limit
	}
	if params.Offset != nil {
		offset = *params.Offset
	}
	if params.TypeCode != nil {
		typeCode = *params.TypeCode
	}

	_, err = pdc.patRepo.Get(domain.Patient{GUID: params.PatientGUID})
	if err == domain.ErrPatientNotFound {
		payload := new(patient.PatientDataCollectionNotFoundBody)
		payload.Version = Version
		payload.Message = NotFoundMessage
		return patient.NewPatientDataCollectionNotFound().WithPayload(payload)
	}

	var dataset []domain.PatientParam
	dataset, err = pdc.paramRepo.List(domain.PatientParamsFilter{
		Limit:  limit,
		Offset: offset,
		Object: domain.PatientParam{
			PatientGUID: params.PatientGUID,
			TypeCode:    typeCode,
		},
	},
	)
	if err == domain.ErrPatientDataNotFound {
		return patientDataCollectionNotFoundResp(err)
	}

	if err != nil {
		return patientDataCollectionInternalServerErrorResp(err)
	}

	var payload = new(patient.PatientDataCollectionOKBody)
	for _, d := range dataset {
		item := patDataToResponse(&d)
		payload.Data = append(payload.Data, item)
	}

	payload.Version = Version
	payload.Message = PayloadSuccessMessage

	return patient.NewPatientDataCollectionOK().WithPayload(payload)
}

func patientDataCollectionInternalServerErrorResp(err error) *patient.PatientDataCollectionInternalServerError {
	payload := new(patient.PatientDataCollectionInternalServerErrorBody)
	payload.Errors = err.Error()
	payload.Version = Version
	payload.Message = PayloadFailMessage
	return patient.NewPatientDataCollectionInternalServerError().WithPayload(payload)
}

func patientDataCollectionNotFoundResp(err error) *patient.PatientDataCollectionNotFound {
	payload := new(patient.PatientDataCollectionNotFoundBody)
	payload.Errors = err.Error()
	payload.Version = Version
	payload.Message = NotFoundMessage
	return patient.NewPatientDataCollectionNotFound().WithPayload(payload)

}

func patDataToResponse(d *domain.PatientParam) *patient.PatientDataItem {
	created := strfmt.DateTime(d.Created)
	var updated strfmt.DateTime
	if d.Updated != nil {
		updated = strfmt.DateTime(*d.Updated)
	}
	item := new(patient.PatientDataItem)
	item.GUID = d.GUID
	item.TypeCode = d.TypeCode
	item.Data = d.Data
	item.Created = &created
	item.Updated = &updated
	return item
}
