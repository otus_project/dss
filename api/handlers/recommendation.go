package handlers

import (
	errs "github.com/go-openapi/errors"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/strfmt"

	"gitlab.com/otus_project/dss/api/models"
	"gitlab.com/otus_project/dss/api/restapi/operations/recommendation"
	"gitlab.com/otus_project/dss/cmd/dss/db/mongod"
	"gitlab.com/otus_project/dss/cmd/dss/domain"
	"gitlab.com/otus_project/dss/logger"
)

type RecommendationController struct {
	recRepo mongod.RecommendationRepository
	l       logger.APILogger
}

// NewRecommendationController is event controller constructor.
func NewRecommendationController(
	rr mongod.RecommendationRepository,
	l logger.APILogger,
) *RecommendationController {
	return &RecommendationController{
		recRepo: rr,
		l:       l,
	}
}

func (rc *RecommendationController) Get(
	params recommendation.RecommendationViewParams,
) middleware.Responder {
	var err error

	var rec *domain.Recommendation
	rec, err = rc.recRepo.Get(domain.Recommendation{
		Code: params.RecommendationCode,
	})
	if err == domain.ErrRecommendationNotFound {
		payload := new(recommendation.RecommendationViewNotFoundBody)
		payload.Version = Version
		payload.Message = NotFoundMessage
		return recommendation.NewRecommendationViewNotFound().WithPayload(payload)
	}

	if err != nil {
		rc.l.DSSDebug(err)
		payload := new(recommendation.RecommendationViewInternalServerErrorBody)
		payload.Errors = err.Error()
		payload.Version = Version
		payload.Message = PayloadFailMessage
		return recommendation.NewRecommendationViewInternalServerError().WithPayload(payload)
	}

	var payload = new(recommendation.RecommendationViewOKBody)
	item := recommendationToResponse(rec)

	payload.Version = Version
	payload.Message = PayloadSuccessMessage
	payload.Data = append(payload.Data, item)

	return recommendation.NewRecommendationViewOK().WithPayload(payload)
}

func (rc *RecommendationController) Post(
	params recommendation.RecommendationCreateParams,
) middleware.Responder {
	var err error
	var rec *domain.Recommendation

	if params.Body.RecommendationObject == (models.RecommendationObject{}) {
		payload := new(recommendation.RecommendationCreateBadRequestBody)
		err := errs.Required("body", "body")
		compErr := errs.CompositeValidationError(err)
		payload.Errors = New400Errors(compErr, params.HTTPRequest)
		payload.Version = Version
		payload.Message = PayloadValidationErrorMessage
		return recommendation.NewRecommendationCreateBadRequest().WithPayload(payload)
	}

	rec, err = rc.recRepo.Set(domain.Recommendation{
		Code:        params.Body.Code,
		GroupCode:   params.Body.GroupCode,
		Condition:   params.Body.Condition,
		Description: params.Body.Description,
	})

	if err == domain.ErrRecommendationAlreadyExists {
		payload := new(recommendation.RecommendationCreateBadRequestBody)
		err := errs.DuplicateItems(params.Body.Code.String(), params.HTTPRequest.RequestURI)
		compErr := errs.CompositeValidationError(err)
		payload.Errors = New400Errors(compErr, params.HTTPRequest)
		payload.Version = Version
		payload.Message = PayloadValidationErrorMessage
		return recommendation.NewRecommendationCreateBadRequest().WithPayload(payload)
	}

	if err != nil {
		rc.l.DSSDebug(err)
		payload := new(recommendation.RecommendationCreateInternalServerErrorBody)
		payload.Errors = err.Error()
		payload.Version = Version
		payload.Message = PayloadFailMessage
		return recommendation.NewRecommendationCreateInternalServerError().WithPayload(payload)
	}

	var payload = new(recommendation.RecommendationCreateOKBody)
	item := recommendationToResponse(rec)

	payload.Version = Version
	payload.Message = PayloadSuccessMessage
	payload.Data = append(payload.Data, item)

	rc.l.DSSInfo(rec.Code.String(), logger.APICREATED)

	return recommendation.NewRecommendationCreateOK().WithPayload(payload)
}

func (rc *RecommendationController) Patch(
	params recommendation.RecommendationUpdateParams,
) middleware.Responder {
	var err error
	var rec *domain.Recommendation

	rec, err = rc.recRepo.Update(
		domain.Recommendation{
			Code: params.RecommendationCode,
		},
		domain.Recommendation{
			GroupCode:   params.Body.GroupCode,
			Condition:   params.Body.Condition,
			Description: params.Body.Description,
		},
	)
	if err == domain.ErrRecommendationNotFound {
		payload := new(recommendation.RecommendationUpdateNotFoundBody)
		payload.Version = Version
		payload.Message = NotFoundMessage
		return recommendation.NewRecommendationUpdateNotFound().WithPayload(payload)
	}

	if err != nil {
		rc.l.DSSDebug(err)
		payload := new(recommendation.RecommendationUpdateInternalServerErrorBody)
		payload.Errors = err.Error()
		payload.Version = Version
		payload.Message = PayloadFailMessage
		return recommendation.NewRecommendationUpdateInternalServerError().WithPayload(payload)
	}

	var payload = new(recommendation.RecommendationUpdateOKBody)
	item := recommendationToResponse(rec)

	payload.Version = Version
	payload.Message = PayloadSuccessMessage
	payload.Data = append(payload.Data, item)

	rc.l.DSSInfo(rec.Code.String(), logger.APIUPDATED)

	return recommendation.NewRecommendationUpdateOK().WithPayload(payload)
}

func (rc *RecommendationController) Delete(
	params recommendation.RecommendationDeleteParams,
) middleware.Responder {
	var err error

	_, err = rc.recRepo.Get(domain.Recommendation{ // fixing bug with empty deleting result
		Code: params.RecommendationCode,
	})
	if err == domain.ErrRecommendationNotFound {
		payload := new(recommendation.RecommendationDeleteNotFoundBody)
		payload.Version = Version
		payload.Message = NotFoundMessage
		return recommendation.NewRecommendationDeleteNotFound().WithPayload(payload)
	}

	err = rc.recRepo.Delete(domain.Recommendation{
		Code: params.RecommendationCode,
	})
	if err != nil {
		rc.l.DSSDebug(err)
		payload := new(recommendation.RecommendationDeleteInternalServerErrorBody)
		payload.Errors = err.Error()
		payload.Version = Version
		payload.Message = PayloadFailMessage
		return recommendation.NewRecommendationDeleteInternalServerError().WithPayload(payload)
	}

	var payload = new(recommendation.RecommendationDeleteOKBody)
	payload.Version = Version
	payload.Data = []interface{}{}
	payload.Message = PayloadSuccessMessage

	rc.l.DSSInfo(params.RecommendationCode.String(), logger.APIDELETED)

	return recommendation.NewRecommendationDeleteOK().WithPayload(payload)

}

func (rc *RecommendationController) List(
	params recommendation.RecommendationCollectionParams,
) middleware.Responder {
	var err error
	var limit int64
	var offset int64

	if params.Limit != nil {
		limit = *params.Limit
	}
	if params.Offset != nil {
		offset = *params.Offset
	}

	var recs []domain.Recommendation
	recs, err = rc.recRepo.List(domain.RecommendationsFilter{
		Limit:  limit,
		Offset: offset,
	},
	)
	if err == domain.ErrRecommendationNotFound {
		return recommendationCollectionNotFoundResp(err)
	}

	if err != nil {
		rc.l.DSSDebug(err)
		return recommendationCollectionInternalServerErrorResp(err)
	}

	var payload = new(recommendation.RecommendationCollectionOKBody)
	for _, r := range recs {
		item := recommendationToResponse(&r)
		payload.Data = append(payload.Data, item)
	}

	payload.Version = Version
	payload.Message = PayloadSuccessMessage

	return recommendation.NewRecommendationCollectionOK().WithPayload(payload)
}

func recommendationCollectionNotFoundResp(err error) *recommendation.RecommendationCollectionNotFound {
	payload := new(recommendation.RecommendationCollectionNotFoundBody)
	payload.Errors = err.Error()
	payload.Version = Version
	payload.Message = NotFoundMessage
	return recommendation.NewRecommendationCollectionNotFound().WithPayload(payload)
}

func recommendationCollectionInternalServerErrorResp(err error) *recommendation.RecommendationCollectionInternalServerError {
	payload := new(recommendation.RecommendationCollectionInternalServerErrorBody)
	payload.Errors = err.Error()
	payload.Version = Version
	payload.Message = PayloadFailMessage
	return recommendation.NewRecommendationCollectionInternalServerError().WithPayload(payload)
}

func recommendationToResponse(rec *domain.Recommendation) *recommendation.DataItems0 {
	created := strfmt.DateTime(rec.Created)
	var updated strfmt.DateTime
	if rec.Updated != nil {
		updated = strfmt.DateTime(*rec.Updated)
	}
	item := new(recommendation.DataItems0)
	item.Code = rec.Code
	item.GroupCode = rec.GroupCode
	item.Condition = rec.Condition
	item.Description = rec.Description
	item.Created = &created
	item.Updated = &updated
	return item
}
