package handlers

import (
	"fmt"
	errs "github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"io"
	"net/http"
	"regexp"
	"gitlab.com/otus_project/dss/api/models"
	"gitlab.com/otus_project/dss/cmd/dss/domain"
	"gitlab.com/otus_project/dss/report"
	"strings"
)

type DSS400Err struct {
	models.Error400Data
}

func (br *DSS400Err) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {
	if err := producer.Produce(rw, br); err != nil {
		panic(err)
	}
}

func ServeDSSError(rw http.ResponseWriter, req *http.Request, err error) {
	var data []string
	var msg string
	var dssErr = new(DSS400Err)

	switch e := err.(type) {
	case *errs.MethodNotAllowedError:
		if req == nil {
			return
		}
		if req.Method == http.MethodHead {
			return
		}

		msg = fmt.Sprintf(
			"Method %v not allowed", req.Method,
		)
		rw.Header().Add("Allow", strings.Join(e.Allowed, ","))
		rw.WriteHeader(asHTTPCode(int(e.Code())))

		dssErr.Version = Version
		dssErr.Data = data
		dssErr.Message = &msg
		dssErr.Errors = New400Errors(nil, req)
		dssErr.WriteResponse(rw, runtime.JSONProducer())

	case *errs.CompositeError:
		rw.WriteHeader(400)

		dssErr.Version = Version
		dssErr.Message = PayloadValidationErrorMessage
		dssErr.Data = data
		dssErr.Errors = New400Errors(e, req)
		dssErr.WriteResponse(rw, runtime.JSONProducer())

	case errs.Error:
		rw.WriteHeader(asHTTPCode(int(e.Code())))

		dssErr.Version = Version
		dssErr.Data = data
		if e.Code() == http.StatusNotFound {
			dssErr.Message = NotFoundMessage
		} else {
			dssErr.Message = InternalServerErrorMessage
		}
		dssErr.Errors = New400Errors(nil, req)
		dssErr.WriteResponse(rw, runtime.JSONProducer())

	case nil:
		rw.WriteHeader(http.StatusInternalServerError)
		dssErr.Version = Version
		dssErr.Data = data
		dssErr.Message = InternalServerErrorMessage
		dssErr.Errors = New400Errors(nil, req)

		dssErr.WriteResponse(rw, runtime.JSONProducer())

	default:
		defer func() {
			if r := recover(); r != nil {
				report.Capture(r, req)
				dssErr.Errors.Core = fmt.Sprint(r)
				dssErr.WriteResponse(rw, runtime.JSONProducer())
			}
		}()

		rw.WriteHeader(http.StatusInternalServerError)
		dssErr.Version = Version
		dssErr.Data = data
		dssErr.Message = InternalServerErrorMessage
		dssErr.Errors = New400Errors(nil, req)

		dssErr.WriteResponse(rw, runtime.JSONProducer())
	}
}

const (
	format   = "format"
	required = "required"
	enum     = "enum"
	unique   = "unique"
)

var (
	errorBadTypePattern    = regexp.MustCompile(`cannot unmarshal \w+ into Go struct field \w+\.(\w+) of type (\w+)`)
	errorRequiredPattern   = regexp.MustCompile(`(\w+) in \w+ is required`)
	errorEnumPattern       = regexp.MustCompile(`(\w+) in \w+ should be one of .*`)
	errorBodyTypePattern   = regexp.MustCompile(`parsing (\w+) .* failed, because parse error: expected (\w+) .*`)
	errorEOFString         = `failed, because unexpected EOF`
	errorTimeString        = `failed, because parsing time`
	errorDuplicationString = `shouldn't contain duplicates`
	errorUUIDString        = `must be of type uuid`
)

func New400Errors(err *errs.CompositeError, r *http.Request) (res *models.Error400DataAO1Errors) {
	res = new(models.Error400DataAO1Errors)
	if err == nil {
		return res
	}

	fields := chooseRepoFields(r)

	for _, subErr := range flattenComposite(err).Errors {
		switch e := subErr.(type) {
		case *errs.ParseError:
			if m := errorBadTypePattern.FindStringSubmatch(e.Error()); m != nil {
				fields.SetFields(m[1], m[2])
			} else if m := errorBodyTypePattern.FindStringSubmatch(e.Error()); m != nil {
				res.Core = "JSON parse error "
				fields.SetFields(m[1], m[2])
			} else if strings.Contains(e.Error(), errorEOFString) {
				res.JSON = io.EOF.Error()
			} else if strings.Contains(e.Error(), errorTimeString) {
				fields.SetFields("body", format)
			} else {
				res.Core = res.Core + e.Error() + "\n"
			}
		case *errs.Validation:
			if m := errorRequiredPattern.FindStringSubmatch(e.Error()); m != nil {
				fields.SetFields(m[1], required)
			} else if strings.Contains(e.Error(), errorDuplicationString) {
				for k := range fields {
					if k == "GUID" || k == "code" {
						fields[k] = unique
					}
				}
			} else if strings.Contains(e.Error(), errorUUIDString) {
				for k := range fields {
					if k == "GUID" || k == "code" {
						fields[k] = format
					}
				}
			} else if m := errorEnumPattern.FindStringSubmatch(e.Error()); m != nil {
				fields.SetFields(m[1], enum)
			} else {
				res.Core = res.Core + e.Error() + "\n"
			}
		default:
			res.Core = res.Core + e.Error() + "\n"
		}
	}
	res.Validation = fields

	return
}

func flattenComposite(errors *errs.CompositeError) *errs.CompositeError {
	var res []error
	for _, er := range errors.Errors {
		switch e := er.(type) {
		case *errs.CompositeError:
			if len(e.Errors) > 0 {
				flat := flattenComposite(e)
				if len(flat.Errors) > 0 {
					res = append(res, flat.Errors...)
				}
			}
		default:
			if e != nil {
				res = append(res, e)
			}
		}
	}
	return errs.CompositeValidationError(res...)
}

func asHTTPCode(input int) int {
	if input >= 600 {
		return 422
	}
	return input
}

func chooseRepoFields(r *http.Request) fieldValidator {
	var validationFields fieldValidator

	switch {
	case strings.Contains(r.RequestURI, domain.PatientsCollectionName):
		validationFields = map[string]string{"GUID": "", "birthDate": "", "genderCode": "", "ckdCode": ""}
	case strings.Contains(r.RequestURI, domain.PatientsCollectionName) &&
		strings.Contains(r.RequestURI, "data"):
		validationFields = map[string]string{"GUID": "", "typeCode": "", "data": ""}
	case strings.Contains(r.RequestURI, domain.SymptomsCollectionName):
		validationFields = map[string]string{"code": "", "condition": "", "description": ""}
	case strings.Contains(r.RequestURI, domain.RecommendationsCollectionName):
		validationFields = map[string]string{"code": "", "groupCode": "", "description": "", "condition": ""}
	}

	return validationFields
}

type fieldValidator map[string]string

func (v fieldValidator) SetFields(field string, rule string) {
	for k := range v {
		if field == k && field != "body" {
			v[field] = rule
		} else {
			v[k] = rule
		}
	}
}
