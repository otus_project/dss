// Code generated by go-swagger; DO NOT EDIT.

package patient

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"
)

// PatientCreateOKCode is the HTTP code returned for type PatientCreateOK
const PatientCreateOKCode int = 200

/*PatientCreateOK Объект пациента

swagger:response patientCreateOK
*/
type PatientCreateOK struct {

	/*
	  In: Body
	*/
	Payload *PatientCreateOKBody `json:"body,omitempty"`
}

// NewPatientCreateOK creates PatientCreateOK with default headers values
func NewPatientCreateOK() *PatientCreateOK {

	return &PatientCreateOK{}
}

// WithPayload adds the payload to the patient create o k response
func (o *PatientCreateOK) WithPayload(payload *PatientCreateOKBody) *PatientCreateOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the patient create o k response
func (o *PatientCreateOK) SetPayload(payload *PatientCreateOKBody) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *PatientCreateOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// PatientCreateBadRequestCode is the HTTP code returned for type PatientCreateBadRequest
const PatientCreateBadRequestCode int = 400

/*PatientCreateBadRequest Validation error

swagger:response patientCreateBadRequest
*/
type PatientCreateBadRequest struct {

	/*
	  In: Body
	*/
	Payload *PatientCreateBadRequestBody `json:"body,omitempty"`
}

// NewPatientCreateBadRequest creates PatientCreateBadRequest with default headers values
func NewPatientCreateBadRequest() *PatientCreateBadRequest {

	return &PatientCreateBadRequest{}
}

// WithPayload adds the payload to the patient create bad request response
func (o *PatientCreateBadRequest) WithPayload(payload *PatientCreateBadRequestBody) *PatientCreateBadRequest {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the patient create bad request response
func (o *PatientCreateBadRequest) SetPayload(payload *PatientCreateBadRequestBody) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *PatientCreateBadRequest) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(400)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// PatientCreateMethodNotAllowedCode is the HTTP code returned for type PatientCreateMethodNotAllowed
const PatientCreateMethodNotAllowedCode int = 405

/*PatientCreateMethodNotAllowed Invalid Method

swagger:response patientCreateMethodNotAllowed
*/
type PatientCreateMethodNotAllowed struct {

	/*
	  In: Body
	*/
	Payload *PatientCreateMethodNotAllowedBody `json:"body,omitempty"`
}

// NewPatientCreateMethodNotAllowed creates PatientCreateMethodNotAllowed with default headers values
func NewPatientCreateMethodNotAllowed() *PatientCreateMethodNotAllowed {

	return &PatientCreateMethodNotAllowed{}
}

// WithPayload adds the payload to the patient create method not allowed response
func (o *PatientCreateMethodNotAllowed) WithPayload(payload *PatientCreateMethodNotAllowedBody) *PatientCreateMethodNotAllowed {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the patient create method not allowed response
func (o *PatientCreateMethodNotAllowed) SetPayload(payload *PatientCreateMethodNotAllowedBody) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *PatientCreateMethodNotAllowed) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(405)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// PatientCreateInternalServerErrorCode is the HTTP code returned for type PatientCreateInternalServerError
const PatientCreateInternalServerErrorCode int = 500

/*PatientCreateInternalServerError Internal Server Error

swagger:response patientCreateInternalServerError
*/
type PatientCreateInternalServerError struct {

	/*
	  In: Body
	*/
	Payload *PatientCreateInternalServerErrorBody `json:"body,omitempty"`
}

// NewPatientCreateInternalServerError creates PatientCreateInternalServerError with default headers values
func NewPatientCreateInternalServerError() *PatientCreateInternalServerError {

	return &PatientCreateInternalServerError{}
}

// WithPayload adds the payload to the patient create internal server error response
func (o *PatientCreateInternalServerError) WithPayload(payload *PatientCreateInternalServerErrorBody) *PatientCreateInternalServerError {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the patient create internal server error response
func (o *PatientCreateInternalServerError) SetPayload(payload *PatientCreateInternalServerErrorBody) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *PatientCreateInternalServerError) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(500)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
