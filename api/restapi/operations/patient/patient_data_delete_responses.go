// Code generated by go-swagger; DO NOT EDIT.

package patient

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"
)

// PatientDataDeleteOKCode is the HTTP code returned for type PatientDataDeleteOK
const PatientDataDeleteOKCode int = 200

/*PatientDataDeleteOK Success

swagger:response patientDataDeleteOK
*/
type PatientDataDeleteOK struct {

	/*
	  In: Body
	*/
	Payload *PatientDataDeleteOKBody `json:"body,omitempty"`
}

// NewPatientDataDeleteOK creates PatientDataDeleteOK with default headers values
func NewPatientDataDeleteOK() *PatientDataDeleteOK {

	return &PatientDataDeleteOK{}
}

// WithPayload adds the payload to the patient data delete o k response
func (o *PatientDataDeleteOK) WithPayload(payload *PatientDataDeleteOKBody) *PatientDataDeleteOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the patient data delete o k response
func (o *PatientDataDeleteOK) SetPayload(payload *PatientDataDeleteOKBody) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *PatientDataDeleteOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// PatientDataDeleteBadRequestCode is the HTTP code returned for type PatientDataDeleteBadRequest
const PatientDataDeleteBadRequestCode int = 400

/*PatientDataDeleteBadRequest Validation error

swagger:response patientDataDeleteBadRequest
*/
type PatientDataDeleteBadRequest struct {

	/*
	  In: Body
	*/
	Payload *PatientDataDeleteBadRequestBody `json:"body,omitempty"`
}

// NewPatientDataDeleteBadRequest creates PatientDataDeleteBadRequest with default headers values
func NewPatientDataDeleteBadRequest() *PatientDataDeleteBadRequest {

	return &PatientDataDeleteBadRequest{}
}

// WithPayload adds the payload to the patient data delete bad request response
func (o *PatientDataDeleteBadRequest) WithPayload(payload *PatientDataDeleteBadRequestBody) *PatientDataDeleteBadRequest {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the patient data delete bad request response
func (o *PatientDataDeleteBadRequest) SetPayload(payload *PatientDataDeleteBadRequestBody) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *PatientDataDeleteBadRequest) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(400)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// PatientDataDeleteNotFoundCode is the HTTP code returned for type PatientDataDeleteNotFound
const PatientDataDeleteNotFoundCode int = 404

/*PatientDataDeleteNotFound Not found

swagger:response patientDataDeleteNotFound
*/
type PatientDataDeleteNotFound struct {

	/*
	  In: Body
	*/
	Payload *PatientDataDeleteNotFoundBody `json:"body,omitempty"`
}

// NewPatientDataDeleteNotFound creates PatientDataDeleteNotFound with default headers values
func NewPatientDataDeleteNotFound() *PatientDataDeleteNotFound {

	return &PatientDataDeleteNotFound{}
}

// WithPayload adds the payload to the patient data delete not found response
func (o *PatientDataDeleteNotFound) WithPayload(payload *PatientDataDeleteNotFoundBody) *PatientDataDeleteNotFound {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the patient data delete not found response
func (o *PatientDataDeleteNotFound) SetPayload(payload *PatientDataDeleteNotFoundBody) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *PatientDataDeleteNotFound) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(404)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// PatientDataDeleteMethodNotAllowedCode is the HTTP code returned for type PatientDataDeleteMethodNotAllowed
const PatientDataDeleteMethodNotAllowedCode int = 405

/*PatientDataDeleteMethodNotAllowed Invalid Method

swagger:response patientDataDeleteMethodNotAllowed
*/
type PatientDataDeleteMethodNotAllowed struct {

	/*
	  In: Body
	*/
	Payload *PatientDataDeleteMethodNotAllowedBody `json:"body,omitempty"`
}

// NewPatientDataDeleteMethodNotAllowed creates PatientDataDeleteMethodNotAllowed with default headers values
func NewPatientDataDeleteMethodNotAllowed() *PatientDataDeleteMethodNotAllowed {

	return &PatientDataDeleteMethodNotAllowed{}
}

// WithPayload adds the payload to the patient data delete method not allowed response
func (o *PatientDataDeleteMethodNotAllowed) WithPayload(payload *PatientDataDeleteMethodNotAllowedBody) *PatientDataDeleteMethodNotAllowed {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the patient data delete method not allowed response
func (o *PatientDataDeleteMethodNotAllowed) SetPayload(payload *PatientDataDeleteMethodNotAllowedBody) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *PatientDataDeleteMethodNotAllowed) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(405)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// PatientDataDeleteInternalServerErrorCode is the HTTP code returned for type PatientDataDeleteInternalServerError
const PatientDataDeleteInternalServerErrorCode int = 500

/*PatientDataDeleteInternalServerError Internal Server Error

swagger:response patientDataDeleteInternalServerError
*/
type PatientDataDeleteInternalServerError struct {

	/*
	  In: Body
	*/
	Payload *PatientDataDeleteInternalServerErrorBody `json:"body,omitempty"`
}

// NewPatientDataDeleteInternalServerError creates PatientDataDeleteInternalServerError with default headers values
func NewPatientDataDeleteInternalServerError() *PatientDataDeleteInternalServerError {

	return &PatientDataDeleteInternalServerError{}
}

// WithPayload adds the payload to the patient data delete internal server error response
func (o *PatientDataDeleteInternalServerError) WithPayload(payload *PatientDataDeleteInternalServerErrorBody) *PatientDataDeleteInternalServerError {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the patient data delete internal server error response
func (o *PatientDataDeleteInternalServerError) SetPayload(payload *PatientDataDeleteInternalServerErrorBody) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *PatientDataDeleteInternalServerError) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(500)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
