// Code generated by go-swagger; DO NOT EDIT.

package symptom

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"
)

// SymptomCreateOKCode is the HTTP code returned for type SymptomCreateOK
const SymptomCreateOKCode int = 200

/*SymptomCreateOK Объект признака

swagger:response symptomCreateOK
*/
type SymptomCreateOK struct {

	/*
	  In: Body
	*/
	Payload *SymptomCreateOKBody `json:"body,omitempty"`
}

// NewSymptomCreateOK creates SymptomCreateOK with default headers values
func NewSymptomCreateOK() *SymptomCreateOK {

	return &SymptomCreateOK{}
}

// WithPayload adds the payload to the symptom create o k response
func (o *SymptomCreateOK) WithPayload(payload *SymptomCreateOKBody) *SymptomCreateOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the symptom create o k response
func (o *SymptomCreateOK) SetPayload(payload *SymptomCreateOKBody) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *SymptomCreateOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// SymptomCreateBadRequestCode is the HTTP code returned for type SymptomCreateBadRequest
const SymptomCreateBadRequestCode int = 400

/*SymptomCreateBadRequest Validation error

swagger:response symptomCreateBadRequest
*/
type SymptomCreateBadRequest struct {

	/*
	  In: Body
	*/
	Payload *SymptomCreateBadRequestBody `json:"body,omitempty"`
}

// NewSymptomCreateBadRequest creates SymptomCreateBadRequest with default headers values
func NewSymptomCreateBadRequest() *SymptomCreateBadRequest {

	return &SymptomCreateBadRequest{}
}

// WithPayload adds the payload to the symptom create bad request response
func (o *SymptomCreateBadRequest) WithPayload(payload *SymptomCreateBadRequestBody) *SymptomCreateBadRequest {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the symptom create bad request response
func (o *SymptomCreateBadRequest) SetPayload(payload *SymptomCreateBadRequestBody) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *SymptomCreateBadRequest) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(400)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// SymptomCreateMethodNotAllowedCode is the HTTP code returned for type SymptomCreateMethodNotAllowed
const SymptomCreateMethodNotAllowedCode int = 405

/*SymptomCreateMethodNotAllowed Invalid Method

swagger:response symptomCreateMethodNotAllowed
*/
type SymptomCreateMethodNotAllowed struct {

	/*
	  In: Body
	*/
	Payload *SymptomCreateMethodNotAllowedBody `json:"body,omitempty"`
}

// NewSymptomCreateMethodNotAllowed creates SymptomCreateMethodNotAllowed with default headers values
func NewSymptomCreateMethodNotAllowed() *SymptomCreateMethodNotAllowed {

	return &SymptomCreateMethodNotAllowed{}
}

// WithPayload adds the payload to the symptom create method not allowed response
func (o *SymptomCreateMethodNotAllowed) WithPayload(payload *SymptomCreateMethodNotAllowedBody) *SymptomCreateMethodNotAllowed {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the symptom create method not allowed response
func (o *SymptomCreateMethodNotAllowed) SetPayload(payload *SymptomCreateMethodNotAllowedBody) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *SymptomCreateMethodNotAllowed) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(405)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// SymptomCreateInternalServerErrorCode is the HTTP code returned for type SymptomCreateInternalServerError
const SymptomCreateInternalServerErrorCode int = 500

/*SymptomCreateInternalServerError Internal Server Error

swagger:response symptomCreateInternalServerError
*/
type SymptomCreateInternalServerError struct {

	/*
	  In: Body
	*/
	Payload *SymptomCreateInternalServerErrorBody `json:"body,omitempty"`
}

// NewSymptomCreateInternalServerError creates SymptomCreateInternalServerError with default headers values
func NewSymptomCreateInternalServerError() *SymptomCreateInternalServerError {

	return &SymptomCreateInternalServerError{}
}

// WithPayload adds the payload to the symptom create internal server error response
func (o *SymptomCreateInternalServerError) WithPayload(payload *SymptomCreateInternalServerErrorBody) *SymptomCreateInternalServerError {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the symptom create internal server error response
func (o *SymptomCreateInternalServerError) SetPayload(payload *SymptomCreateInternalServerErrorBody) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *SymptomCreateInternalServerError) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(500)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
