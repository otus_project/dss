// Code generated by go-swagger; DO NOT EDIT.

package recommendation

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/validate"

	strfmt "github.com/go-openapi/strfmt"
)

// NewRecommendationViewParams creates a new RecommendationViewParams object
// no default values defined in spec.
func NewRecommendationViewParams() RecommendationViewParams {

	return RecommendationViewParams{}
}

// RecommendationViewParams contains all the bound params for the recommendation view operation
// typically these are obtained from a http.Request
//
// swagger:parameters RecommendationView
type RecommendationViewParams struct {

	// HTTP Request Object
	HTTPRequest *http.Request `json:"-"`

	/*GUID рекоммендации
	  Required: true
	  In: path
	*/
	RecommendationCode strfmt.UUID
}

// BindRequest both binds and validates a request, it assumes that complex things implement a Validatable(strfmt.Registry) error interface
// for simple values it will use straight method calls.
//
// To ensure default values, the struct must have been initialized with NewRecommendationViewParams() beforehand.
func (o *RecommendationViewParams) BindRequest(r *http.Request, route *middleware.MatchedRoute) error {
	var res []error

	o.HTTPRequest = r

	rRecommendationCode, rhkRecommendationCode, _ := route.Params.GetOK("recommendationCode")
	if err := o.bindRecommendationCode(rRecommendationCode, rhkRecommendationCode, route.Formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// bindRecommendationCode binds and validates parameter RecommendationCode from path.
func (o *RecommendationViewParams) bindRecommendationCode(rawData []string, hasKey bool, formats strfmt.Registry) error {
	var raw string
	if len(rawData) > 0 {
		raw = rawData[len(rawData)-1]
	}

	// Required: true
	// Parameter is provided by construction from the route

	// Format: uuid
	value, err := formats.Parse("uuid", raw)
	if err != nil {
		return errors.InvalidType("recommendationCode", "path", "strfmt.UUID", raw)
	}
	o.RecommendationCode = *(value.(*strfmt.UUID))

	if err := o.validateRecommendationCode(formats); err != nil {
		return err
	}

	return nil
}

// validateRecommendationCode carries on validations for parameter RecommendationCode
func (o *RecommendationViewParams) validateRecommendationCode(formats strfmt.Registry) error {

	if err := validate.FormatOf("recommendationCode", "path", "uuid", o.RecommendationCode.String(), formats); err != nil {
		return err
	}
	return nil
}
