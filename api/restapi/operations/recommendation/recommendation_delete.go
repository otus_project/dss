// Code generated by go-swagger; DO NOT EDIT.

package recommendation

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	errors "github.com/go-openapi/errors"
	middleware "github.com/go-openapi/runtime/middleware"
	strfmt "github.com/go-openapi/strfmt"
	swag "github.com/go-openapi/swag"

	models "gitlab.com/otus_project/dss/api/models"
)

// RecommendationDeleteHandlerFunc turns a function with the right signature into a recommendation delete handler
type RecommendationDeleteHandlerFunc func(RecommendationDeleteParams) middleware.Responder

// Handle executing the request and returning a response
func (fn RecommendationDeleteHandlerFunc) Handle(params RecommendationDeleteParams) middleware.Responder {
	return fn(params)
}

// RecommendationDeleteHandler interface for that can handle valid recommendation delete params
type RecommendationDeleteHandler interface {
	Handle(RecommendationDeleteParams) middleware.Responder
}

// NewRecommendationDelete creates a new http.Handler for the recommendation delete operation
func NewRecommendationDelete(ctx *middleware.Context, handler RecommendationDeleteHandler) *RecommendationDelete {
	return &RecommendationDelete{Context: ctx, Handler: handler}
}

/*RecommendationDelete swagger:route DELETE /recommendations/{recommendationCode} Recommendation recommendationDelete

Удаление правила рекоммендации

*/
type RecommendationDelete struct {
	Context *middleware.Context
	Handler RecommendationDeleteHandler
}

func (o *RecommendationDelete) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewRecommendationDeleteParams()

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}

// RecommendationDeleteBadRequestBody recommendation delete bad request body
// swagger:model RecommendationDeleteBadRequestBody
type RecommendationDeleteBadRequestBody struct {
	models.Error400Data
}

// UnmarshalJSON unmarshals this object from a JSON structure
func (o *RecommendationDeleteBadRequestBody) UnmarshalJSON(raw []byte) error {
	// RecommendationDeleteBadRequestBodyAO0
	var recommendationDeleteBadRequestBodyAO0 models.Error400Data
	if err := swag.ReadJSON(raw, &recommendationDeleteBadRequestBodyAO0); err != nil {
		return err
	}
	o.Error400Data = recommendationDeleteBadRequestBodyAO0

	return nil
}

// MarshalJSON marshals this object to a JSON structure
func (o RecommendationDeleteBadRequestBody) MarshalJSON() ([]byte, error) {
	_parts := make([][]byte, 0, 1)

	recommendationDeleteBadRequestBodyAO0, err := swag.WriteJSON(o.Error400Data)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, recommendationDeleteBadRequestBodyAO0)

	return swag.ConcatJSON(_parts...), nil
}

// Validate validates this recommendation delete bad request body
func (o *RecommendationDeleteBadRequestBody) Validate(formats strfmt.Registry) error {
	var res []error

	// validation for a type composition with models.Error400Data
	if err := o.Error400Data.Validate(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (o *RecommendationDeleteBadRequestBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *RecommendationDeleteBadRequestBody) UnmarshalBinary(b []byte) error {
	var res RecommendationDeleteBadRequestBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

// RecommendationDeleteInternalServerErrorBody recommendation delete internal server error body
// swagger:model RecommendationDeleteInternalServerErrorBody
type RecommendationDeleteInternalServerErrorBody struct {
	models.Error500Data
}

// UnmarshalJSON unmarshals this object from a JSON structure
func (o *RecommendationDeleteInternalServerErrorBody) UnmarshalJSON(raw []byte) error {
	// RecommendationDeleteInternalServerErrorBodyAO0
	var recommendationDeleteInternalServerErrorBodyAO0 models.Error500Data
	if err := swag.ReadJSON(raw, &recommendationDeleteInternalServerErrorBodyAO0); err != nil {
		return err
	}
	o.Error500Data = recommendationDeleteInternalServerErrorBodyAO0

	return nil
}

// MarshalJSON marshals this object to a JSON structure
func (o RecommendationDeleteInternalServerErrorBody) MarshalJSON() ([]byte, error) {
	_parts := make([][]byte, 0, 1)

	recommendationDeleteInternalServerErrorBodyAO0, err := swag.WriteJSON(o.Error500Data)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, recommendationDeleteInternalServerErrorBodyAO0)

	return swag.ConcatJSON(_parts...), nil
}

// Validate validates this recommendation delete internal server error body
func (o *RecommendationDeleteInternalServerErrorBody) Validate(formats strfmt.Registry) error {
	var res []error

	// validation for a type composition with models.Error500Data
	if err := o.Error500Data.Validate(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (o *RecommendationDeleteInternalServerErrorBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *RecommendationDeleteInternalServerErrorBody) UnmarshalBinary(b []byte) error {
	var res RecommendationDeleteInternalServerErrorBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

// RecommendationDeleteMethodNotAllowedBody recommendation delete method not allowed body
// swagger:model RecommendationDeleteMethodNotAllowedBody
type RecommendationDeleteMethodNotAllowedBody struct {
	models.Error405Data
}

// UnmarshalJSON unmarshals this object from a JSON structure
func (o *RecommendationDeleteMethodNotAllowedBody) UnmarshalJSON(raw []byte) error {
	// RecommendationDeleteMethodNotAllowedBodyAO0
	var recommendationDeleteMethodNotAllowedBodyAO0 models.Error405Data
	if err := swag.ReadJSON(raw, &recommendationDeleteMethodNotAllowedBodyAO0); err != nil {
		return err
	}
	o.Error405Data = recommendationDeleteMethodNotAllowedBodyAO0

	return nil
}

// MarshalJSON marshals this object to a JSON structure
func (o RecommendationDeleteMethodNotAllowedBody) MarshalJSON() ([]byte, error) {
	_parts := make([][]byte, 0, 1)

	recommendationDeleteMethodNotAllowedBodyAO0, err := swag.WriteJSON(o.Error405Data)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, recommendationDeleteMethodNotAllowedBodyAO0)

	return swag.ConcatJSON(_parts...), nil
}

// Validate validates this recommendation delete method not allowed body
func (o *RecommendationDeleteMethodNotAllowedBody) Validate(formats strfmt.Registry) error {
	var res []error

	// validation for a type composition with models.Error405Data
	if err := o.Error405Data.Validate(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (o *RecommendationDeleteMethodNotAllowedBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *RecommendationDeleteMethodNotAllowedBody) UnmarshalBinary(b []byte) error {
	var res RecommendationDeleteMethodNotAllowedBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

// RecommendationDeleteNotFoundBody recommendation delete not found body
// swagger:model RecommendationDeleteNotFoundBody
type RecommendationDeleteNotFoundBody struct {
	models.Error404Data
}

// UnmarshalJSON unmarshals this object from a JSON structure
func (o *RecommendationDeleteNotFoundBody) UnmarshalJSON(raw []byte) error {
	// RecommendationDeleteNotFoundBodyAO0
	var recommendationDeleteNotFoundBodyAO0 models.Error404Data
	if err := swag.ReadJSON(raw, &recommendationDeleteNotFoundBodyAO0); err != nil {
		return err
	}
	o.Error404Data = recommendationDeleteNotFoundBodyAO0

	return nil
}

// MarshalJSON marshals this object to a JSON structure
func (o RecommendationDeleteNotFoundBody) MarshalJSON() ([]byte, error) {
	_parts := make([][]byte, 0, 1)

	recommendationDeleteNotFoundBodyAO0, err := swag.WriteJSON(o.Error404Data)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, recommendationDeleteNotFoundBodyAO0)

	return swag.ConcatJSON(_parts...), nil
}

// Validate validates this recommendation delete not found body
func (o *RecommendationDeleteNotFoundBody) Validate(formats strfmt.Registry) error {
	var res []error

	// validation for a type composition with models.Error404Data
	if err := o.Error404Data.Validate(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (o *RecommendationDeleteNotFoundBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *RecommendationDeleteNotFoundBody) UnmarshalBinary(b []byte) error {
	var res RecommendationDeleteNotFoundBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

// RecommendationDeleteOKBody recommendation delete o k body
// swagger:model RecommendationDeleteOKBody
type RecommendationDeleteOKBody struct {
	models.SuccessData

	// data
	Data []interface{} `json:"data"`
}

// UnmarshalJSON unmarshals this object from a JSON structure
func (o *RecommendationDeleteOKBody) UnmarshalJSON(raw []byte) error {
	// RecommendationDeleteOKBodyAO0
	var recommendationDeleteOKBodyAO0 models.SuccessData
	if err := swag.ReadJSON(raw, &recommendationDeleteOKBodyAO0); err != nil {
		return err
	}
	o.SuccessData = recommendationDeleteOKBodyAO0

	// RecommendationDeleteOKBodyAO1
	var dataRecommendationDeleteOKBodyAO1 struct {
		Data []interface{} `json:"data"`
	}
	if err := swag.ReadJSON(raw, &dataRecommendationDeleteOKBodyAO1); err != nil {
		return err
	}

	o.Data = dataRecommendationDeleteOKBodyAO1.Data

	return nil
}

// MarshalJSON marshals this object to a JSON structure
func (o RecommendationDeleteOKBody) MarshalJSON() ([]byte, error) {
	_parts := make([][]byte, 0, 2)

	recommendationDeleteOKBodyAO0, err := swag.WriteJSON(o.SuccessData)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, recommendationDeleteOKBodyAO0)

	var dataRecommendationDeleteOKBodyAO1 struct {
		Data []interface{} `json:"data"`
	}

	dataRecommendationDeleteOKBodyAO1.Data = o.Data

	jsonDataRecommendationDeleteOKBodyAO1, errRecommendationDeleteOKBodyAO1 := swag.WriteJSON(dataRecommendationDeleteOKBodyAO1)
	if errRecommendationDeleteOKBodyAO1 != nil {
		return nil, errRecommendationDeleteOKBodyAO1
	}
	_parts = append(_parts, jsonDataRecommendationDeleteOKBodyAO1)

	return swag.ConcatJSON(_parts...), nil
}

// Validate validates this recommendation delete o k body
func (o *RecommendationDeleteOKBody) Validate(formats strfmt.Registry) error {
	var res []error

	// validation for a type composition with models.SuccessData
	if err := o.SuccessData.Validate(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (o *RecommendationDeleteOKBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *RecommendationDeleteOKBody) UnmarshalBinary(b []byte) error {
	var res RecommendationDeleteOKBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
