// Code generated by go-swagger; DO NOT EDIT.

package recommendation

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"
	"strconv"

	errors "github.com/go-openapi/errors"
	middleware "github.com/go-openapi/runtime/middleware"
	strfmt "github.com/go-openapi/strfmt"
	swag "github.com/go-openapi/swag"
	validate "github.com/go-openapi/validate"

	models "gitlab.com/otus_project/dss/api/models"
)

// RecommendationCollectionHandlerFunc turns a function with the right signature into a recommendation collection handler
type RecommendationCollectionHandlerFunc func(RecommendationCollectionParams) middleware.Responder

// Handle executing the request and returning a response
func (fn RecommendationCollectionHandlerFunc) Handle(params RecommendationCollectionParams) middleware.Responder {
	return fn(params)
}

// RecommendationCollectionHandler interface for that can handle valid recommendation collection params
type RecommendationCollectionHandler interface {
	Handle(RecommendationCollectionParams) middleware.Responder
}

// NewRecommendationCollection creates a new http.Handler for the recommendation collection operation
func NewRecommendationCollection(ctx *middleware.Context, handler RecommendationCollectionHandler) *RecommendationCollection {
	return &RecommendationCollection{Context: ctx, Handler: handler}
}

/*RecommendationCollection swagger:route GET /recommendations Recommendation recommendationCollection

Коллекция правил рекоммендаций

*/
type RecommendationCollection struct {
	Context *middleware.Context
	Handler RecommendationCollectionHandler
}

func (o *RecommendationCollection) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewRecommendationCollectionParams()

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}

// DataItems0 data items0
// swagger:model DataItems0
type DataItems0 struct {
	models.CodeData

	models.RecommendationObject

	// Дата-время добавления рекоммендации
	// Required: true
	// Format: date-time
	Created *strfmt.DateTime `json:"created"`

	// Дата-время обновления рекоммендации
	// Required: true
	// Format: date-time
	Updated *strfmt.DateTime `json:"updated"`
}

// UnmarshalJSON unmarshals this object from a JSON structure
func (o *DataItems0) UnmarshalJSON(raw []byte) error {
	// AO0
	var aO0 models.CodeData
	if err := swag.ReadJSON(raw, &aO0); err != nil {
		return err
	}
	o.CodeData = aO0

	// AO1
	var aO1 models.RecommendationObject
	if err := swag.ReadJSON(raw, &aO1); err != nil {
		return err
	}
	o.RecommendationObject = aO1

	// AO2
	var dataAO2 struct {
		Created *strfmt.DateTime `json:"created"`

		Updated *strfmt.DateTime `json:"updated"`
	}
	if err := swag.ReadJSON(raw, &dataAO2); err != nil {
		return err
	}

	o.Created = dataAO2.Created

	o.Updated = dataAO2.Updated

	return nil
}

// MarshalJSON marshals this object to a JSON structure
func (o DataItems0) MarshalJSON() ([]byte, error) {
	_parts := make([][]byte, 0, 3)

	aO0, err := swag.WriteJSON(o.CodeData)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, aO0)

	aO1, err := swag.WriteJSON(o.RecommendationObject)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, aO1)

	var dataAO2 struct {
		Created *strfmt.DateTime `json:"created"`

		Updated *strfmt.DateTime `json:"updated"`
	}

	dataAO2.Created = o.Created

	dataAO2.Updated = o.Updated

	jsonDataAO2, errAO2 := swag.WriteJSON(dataAO2)
	if errAO2 != nil {
		return nil, errAO2
	}
	_parts = append(_parts, jsonDataAO2)

	return swag.ConcatJSON(_parts...), nil
}

// Validate validates this data items0
func (o *DataItems0) Validate(formats strfmt.Registry) error {
	var res []error

	// validation for a type composition with models.CodeData
	if err := o.CodeData.Validate(formats); err != nil {
		res = append(res, err)
	}
	// validation for a type composition with models.RecommendationObject
	if err := o.RecommendationObject.Validate(formats); err != nil {
		res = append(res, err)
	}

	if err := o.validateCreated(formats); err != nil {
		res = append(res, err)
	}

	if err := o.validateUpdated(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *DataItems0) validateCreated(formats strfmt.Registry) error {

	if err := validate.Required("created", "body", o.Created); err != nil {
		return err
	}

	if err := validate.FormatOf("created", "body", "date-time", o.Created.String(), formats); err != nil {
		return err
	}

	return nil
}

func (o *DataItems0) validateUpdated(formats strfmt.Registry) error {

	if err := validate.Required("updated", "body", o.Updated); err != nil {
		return err
	}

	if err := validate.FormatOf("updated", "body", "date-time", o.Updated.String(), formats); err != nil {
		return err
	}

	return nil
}

// MarshalBinary interface implementation
func (o *DataItems0) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *DataItems0) UnmarshalBinary(b []byte) error {
	var res DataItems0
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

// RecommendationCollectionBadRequestBody recommendation collection bad request body
// swagger:model RecommendationCollectionBadRequestBody
type RecommendationCollectionBadRequestBody struct {
	models.Error400Data
}

// UnmarshalJSON unmarshals this object from a JSON structure
func (o *RecommendationCollectionBadRequestBody) UnmarshalJSON(raw []byte) error {
	// RecommendationCollectionBadRequestBodyAO0
	var recommendationCollectionBadRequestBodyAO0 models.Error400Data
	if err := swag.ReadJSON(raw, &recommendationCollectionBadRequestBodyAO0); err != nil {
		return err
	}
	o.Error400Data = recommendationCollectionBadRequestBodyAO0

	return nil
}

// MarshalJSON marshals this object to a JSON structure
func (o RecommendationCollectionBadRequestBody) MarshalJSON() ([]byte, error) {
	_parts := make([][]byte, 0, 1)

	recommendationCollectionBadRequestBodyAO0, err := swag.WriteJSON(o.Error400Data)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, recommendationCollectionBadRequestBodyAO0)

	return swag.ConcatJSON(_parts...), nil
}

// Validate validates this recommendation collection bad request body
func (o *RecommendationCollectionBadRequestBody) Validate(formats strfmt.Registry) error {
	var res []error

	// validation for a type composition with models.Error400Data
	if err := o.Error400Data.Validate(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (o *RecommendationCollectionBadRequestBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *RecommendationCollectionBadRequestBody) UnmarshalBinary(b []byte) error {
	var res RecommendationCollectionBadRequestBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

// RecommendationCollectionInternalServerErrorBody recommendation collection internal server error body
// swagger:model RecommendationCollectionInternalServerErrorBody
type RecommendationCollectionInternalServerErrorBody struct {
	models.Error500Data
}

// UnmarshalJSON unmarshals this object from a JSON structure
func (o *RecommendationCollectionInternalServerErrorBody) UnmarshalJSON(raw []byte) error {
	// RecommendationCollectionInternalServerErrorBodyAO0
	var recommendationCollectionInternalServerErrorBodyAO0 models.Error500Data
	if err := swag.ReadJSON(raw, &recommendationCollectionInternalServerErrorBodyAO0); err != nil {
		return err
	}
	o.Error500Data = recommendationCollectionInternalServerErrorBodyAO0

	return nil
}

// MarshalJSON marshals this object to a JSON structure
func (o RecommendationCollectionInternalServerErrorBody) MarshalJSON() ([]byte, error) {
	_parts := make([][]byte, 0, 1)

	recommendationCollectionInternalServerErrorBodyAO0, err := swag.WriteJSON(o.Error500Data)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, recommendationCollectionInternalServerErrorBodyAO0)

	return swag.ConcatJSON(_parts...), nil
}

// Validate validates this recommendation collection internal server error body
func (o *RecommendationCollectionInternalServerErrorBody) Validate(formats strfmt.Registry) error {
	var res []error

	// validation for a type composition with models.Error500Data
	if err := o.Error500Data.Validate(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (o *RecommendationCollectionInternalServerErrorBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *RecommendationCollectionInternalServerErrorBody) UnmarshalBinary(b []byte) error {
	var res RecommendationCollectionInternalServerErrorBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

// RecommendationCollectionMethodNotAllowedBody recommendation collection method not allowed body
// swagger:model RecommendationCollectionMethodNotAllowedBody
type RecommendationCollectionMethodNotAllowedBody struct {
	models.Error405Data
}

// UnmarshalJSON unmarshals this object from a JSON structure
func (o *RecommendationCollectionMethodNotAllowedBody) UnmarshalJSON(raw []byte) error {
	// RecommendationCollectionMethodNotAllowedBodyAO0
	var recommendationCollectionMethodNotAllowedBodyAO0 models.Error405Data
	if err := swag.ReadJSON(raw, &recommendationCollectionMethodNotAllowedBodyAO0); err != nil {
		return err
	}
	o.Error405Data = recommendationCollectionMethodNotAllowedBodyAO0

	return nil
}

// MarshalJSON marshals this object to a JSON structure
func (o RecommendationCollectionMethodNotAllowedBody) MarshalJSON() ([]byte, error) {
	_parts := make([][]byte, 0, 1)

	recommendationCollectionMethodNotAllowedBodyAO0, err := swag.WriteJSON(o.Error405Data)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, recommendationCollectionMethodNotAllowedBodyAO0)

	return swag.ConcatJSON(_parts...), nil
}

// Validate validates this recommendation collection method not allowed body
func (o *RecommendationCollectionMethodNotAllowedBody) Validate(formats strfmt.Registry) error {
	var res []error

	// validation for a type composition with models.Error405Data
	if err := o.Error405Data.Validate(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (o *RecommendationCollectionMethodNotAllowedBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *RecommendationCollectionMethodNotAllowedBody) UnmarshalBinary(b []byte) error {
	var res RecommendationCollectionMethodNotAllowedBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

// RecommendationCollectionNotFoundBody recommendation collection not found body
// swagger:model RecommendationCollectionNotFoundBody
type RecommendationCollectionNotFoundBody struct {
	models.Error404Data
}

// UnmarshalJSON unmarshals this object from a JSON structure
func (o *RecommendationCollectionNotFoundBody) UnmarshalJSON(raw []byte) error {
	// RecommendationCollectionNotFoundBodyAO0
	var recommendationCollectionNotFoundBodyAO0 models.Error404Data
	if err := swag.ReadJSON(raw, &recommendationCollectionNotFoundBodyAO0); err != nil {
		return err
	}
	o.Error404Data = recommendationCollectionNotFoundBodyAO0

	return nil
}

// MarshalJSON marshals this object to a JSON structure
func (o RecommendationCollectionNotFoundBody) MarshalJSON() ([]byte, error) {
	_parts := make([][]byte, 0, 1)

	recommendationCollectionNotFoundBodyAO0, err := swag.WriteJSON(o.Error404Data)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, recommendationCollectionNotFoundBodyAO0)

	return swag.ConcatJSON(_parts...), nil
}

// Validate validates this recommendation collection not found body
func (o *RecommendationCollectionNotFoundBody) Validate(formats strfmt.Registry) error {
	var res []error

	// validation for a type composition with models.Error404Data
	if err := o.Error404Data.Validate(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (o *RecommendationCollectionNotFoundBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *RecommendationCollectionNotFoundBody) UnmarshalBinary(b []byte) error {
	var res RecommendationCollectionNotFoundBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}

// RecommendationCollectionOKBody recommendation collection o k body
// swagger:model RecommendationCollectionOKBody
type RecommendationCollectionOKBody struct {
	models.SuccessData

	// data
	Data []*DataItems0 `json:"data"`
}

// UnmarshalJSON unmarshals this object from a JSON structure
func (o *RecommendationCollectionOKBody) UnmarshalJSON(raw []byte) error {
	// RecommendationCollectionOKBodyAO0
	var recommendationCollectionOKBodyAO0 models.SuccessData
	if err := swag.ReadJSON(raw, &recommendationCollectionOKBodyAO0); err != nil {
		return err
	}
	o.SuccessData = recommendationCollectionOKBodyAO0

	// RecommendationCollectionOKBodyAO1
	var dataRecommendationCollectionOKBodyAO1 struct {
		Data []*DataItems0 `json:"data"`
	}
	if err := swag.ReadJSON(raw, &dataRecommendationCollectionOKBodyAO1); err != nil {
		return err
	}

	o.Data = dataRecommendationCollectionOKBodyAO1.Data

	return nil
}

// MarshalJSON marshals this object to a JSON structure
func (o RecommendationCollectionOKBody) MarshalJSON() ([]byte, error) {
	_parts := make([][]byte, 0, 2)

	recommendationCollectionOKBodyAO0, err := swag.WriteJSON(o.SuccessData)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, recommendationCollectionOKBodyAO0)

	var dataRecommendationCollectionOKBodyAO1 struct {
		Data []*DataItems0 `json:"data"`
	}

	dataRecommendationCollectionOKBodyAO1.Data = o.Data

	jsonDataRecommendationCollectionOKBodyAO1, errRecommendationCollectionOKBodyAO1 := swag.WriteJSON(dataRecommendationCollectionOKBodyAO1)
	if errRecommendationCollectionOKBodyAO1 != nil {
		return nil, errRecommendationCollectionOKBodyAO1
	}
	_parts = append(_parts, jsonDataRecommendationCollectionOKBodyAO1)

	return swag.ConcatJSON(_parts...), nil
}

// Validate validates this recommendation collection o k body
func (o *RecommendationCollectionOKBody) Validate(formats strfmt.Registry) error {
	var res []error

	// validation for a type composition with models.SuccessData
	if err := o.SuccessData.Validate(formats); err != nil {
		res = append(res, err)
	}

	if err := o.validateData(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (o *RecommendationCollectionOKBody) validateData(formats strfmt.Registry) error {

	if swag.IsZero(o.Data) { // not required
		return nil
	}

	for i := 0; i < len(o.Data); i++ {
		if swag.IsZero(o.Data[i]) { // not required
			continue
		}

		if o.Data[i] != nil {
			if err := o.Data[i].Validate(formats); err != nil {
				if ve, ok := err.(*errors.Validation); ok {
					return ve.ValidateName("recommendationCollectionOK" + "." + "data" + "." + strconv.Itoa(i))
				}
				return err
			}
		}

	}

	return nil
}

// MarshalBinary interface implementation
func (o *RecommendationCollectionOKBody) MarshalBinary() ([]byte, error) {
	if o == nil {
		return nil, nil
	}
	return swag.WriteJSON(o)
}

// UnmarshalBinary interface implementation
func (o *RecommendationCollectionOKBody) UnmarshalBinary(b []byte) error {
	var res RecommendationCollectionOKBody
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*o = res
	return nil
}
