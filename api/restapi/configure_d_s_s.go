// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"github.com/spf13/viper"
	"net/http"
	"gitlab.com/otus_project/dss/api/handlers"
	repos "gitlab.com/otus_project/dss/cmd/dss/db/mongod"
	"gitlab.com/otus_project/dss/db/mongod"
	"gitlab.com/otus_project/dss/logger"
	"gitlab.com/otus_project/dss/report"

	"github.com/go-openapi/runtime"
	"gitlab.com/otus_project/dss/api/restapi/operations"
	"gitlab.com/otus_project/dss/api/restapi/operations/patient"
	"gitlab.com/otus_project/dss/api/restapi/operations/recommendation"
	"gitlab.com/otus_project/dss/api/restapi/operations/status"
	"gitlab.com/otus_project/dss/api/restapi/operations/symptom"
)

//go:generate swagger generate server --target ../api --name  --spec ../docs/swagger.yaml --exclude-main

func configureFlags(api *operations.DSSAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

// Version of service
var Version string

func configureAPI(api *operations.DSSAPI) http.Handler {
	// configure the api here
	//api.ServeError = handlers.ServeError
	api.ServeError = handlers.ServeDSSError
	handlers.Version = Version

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	var l logger.Logger
	l = logger.GetLogger()

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	var storage mongod.Storer
	storage = mongod.GetStorage()

	api.StatusStatusViewHandler = status.StatusViewHandlerFunc(handlers.NewStatusView(storage).Get)

	var patientRepo repos.PatientRepository
	patientRepo = repos.NewPatientRepo(storage)

	var recRepo repos.RecommendationRepository
	recRepo = repos.NewRecommendationRepo(storage)

	var patDataRepo repos.PatientParamsRepository
	patDataRepo = repos.NewPatientParamsRepo(storage)

	var patCtrl *handlers.PatientController
	patCtrl = handlers.NewPatientController(patientRepo, patDataRepo, recRepo, l)

	api.PatientPatientCollectionHandler = patient.PatientCollectionHandlerFunc(patCtrl.List)
	api.PatientPatientCreateHandler = patient.PatientCreateHandlerFunc(patCtrl.Post)
	api.PatientPatientDeleteHandler = patient.PatientDeleteHandlerFunc(patCtrl.Delete)
	api.PatientPatientUpdateHandler = patient.PatientUpdateHandlerFunc(patCtrl.Patch)
	api.PatientPatientViewHandler = patient.PatientViewHandlerFunc(patCtrl.Get)
	api.PatientPatientRecommendationViewHandler = patient.PatientRecommendationViewHandlerFunc(patCtrl.RecsList)

	var patDataCtrl *handlers.PatientParamsController
	patDataCtrl = handlers.NewPatientParamsController(patientRepo, patDataRepo)

	api.PatientPatientDataCollectionHandler = patient.PatientDataCollectionHandlerFunc(patDataCtrl.List)
	api.PatientPatientDataCreateHandler = patient.PatientDataCreateHandlerFunc(patDataCtrl.Post)
	api.PatientPatientDataDeleteHandler = patient.PatientDataDeleteHandlerFunc(patDataCtrl.Delete)
	api.PatientPatientDataUpdateHandler = patient.PatientDataUpdateHandlerFunc(patDataCtrl.Put)

	var recCtrl *handlers.RecommendationController
	recCtrl = handlers.NewRecommendationController(recRepo, l.Api().Recommendation())

	api.RecommendationRecommendationCollectionHandler = recommendation.RecommendationCollectionHandlerFunc(recCtrl.List)
	api.RecommendationRecommendationCreateHandler = recommendation.RecommendationCreateHandlerFunc(recCtrl.Post)
	api.RecommendationRecommendationDeleteHandler = recommendation.RecommendationDeleteHandlerFunc(recCtrl.Delete)
	api.RecommendationRecommendationUpdateHandler = recommendation.RecommendationUpdateHandlerFunc(recCtrl.Patch)
	api.RecommendationRecommendationViewHandler = recommendation.RecommendationViewHandlerFunc(recCtrl.Get)

	var symRepo repos.SymptomRepository
	symRepo = repos.NewSymptomRepo(storage)

	var symCtrl *handlers.SymptomController
	symCtrl = handlers.NewSymptomController(symRepo, l.Api().Symptom())

	api.SymptomSymptomCollectionHandler = symptom.SymptomCollectionHandlerFunc(symCtrl.List)
	api.SymptomSymptomCreateHandler = symptom.SymptomCreateHandlerFunc(symCtrl.Post)
	api.SymptomSymptomDeleteHandler = symptom.SymptomDeleteHandlerFunc(symCtrl.Delete)
	api.SymptomSymptomUpdateHandler = symptom.SymptomUpdateHandlerFunc(symCtrl.Patch)
	api.SymptomSymptomViewHandler = symptom.SymptomViewHandlerFunc(symCtrl.Get)

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	if viper.GetString("sentryDSN") != "" {
		return report.NewRavenHandler(handler)
	}
	return handler
}
