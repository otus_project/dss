// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/swag"
)

// PatientParamObject Patient_param_object
// swagger:model Patient_param_object
type PatientParamObject struct {

	// data
	Data interface{} `json:"data,omitempty"`

	// Кодификатор типа параметра
	TypeCode string `json:"typeCode,omitempty"`
}

// Validate validates this patient param object
func (m *PatientParamObject) Validate(formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *PatientParamObject) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *PatientParamObject) UnmarshalBinary(b []byte) error {
	var res PatientParamObject
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
