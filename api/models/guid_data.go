// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
	"github.com/go-openapi/validate"
)

// GUIDData GUID_data
// swagger:model GUID_data
type GUIDData struct {

	// GUID
	// Format: uuid
	GUID strfmt.UUID `json:"GUID,omitempty"`
}

// Validate validates this GUID data
func (m *GUIDData) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateGUID(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *GUIDData) validateGUID(formats strfmt.Registry) error {

	if swag.IsZero(m.GUID) { // not required
		return nil
	}

	if err := validate.FormatOf("GUID", "body", "uuid", m.GUID.String(), formats); err != nil {
		return err
	}

	return nil
}

// MarshalBinary interface implementation
func (m *GUIDData) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *GUIDData) UnmarshalBinary(b []byte) error {
	var res GUIDData
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
