# Приложение:

## Предназначение:
Подсистема расчета признаков и рекомендаций
## Архитектура:
![](dss_arch.png)
В качестве базы данных используется MongoDB

## Возможности:
- Управление данными пациентов
- Управление правилами расчета признаков и рекомендаций
- Расчёт признаков
- Расчёт рекомендаций

## API Reference:

### Patient
- GET /patients Коллекция пациентов
- POST /patients Создание пациента
- GET /patients/{patientGUID} Информация о пациенте
- PATCH /patients/{patientGUID} Изменение пациента
- DELETE /patients/{patientGUID} Удаление пациента
- GET /patients/{patientGUID}/params Коллекция параметров пациента
- POST /patients/{patientGUID}/params Добавление параметра пациенту
- PUT /patients/{patientGUID}/params/{paramGUID} Изменение параметра пациента
- DELETE /patients/{patientGUID}/params/{paramGUID} Удаление параметра пациента
- GET /patients/{patientGUID}/recommendations Получение рекоммендаций для пациента Symptom

### Symptoms
- GET /symptoms Коллекция правил признаков
- POST /symptoms Создание правила признака
- GET /symptoms/{symptomCode} Информация о правиле признака
- PATCH /symptoms/{symptomCode} Изменение правила признака
- DELETE /symptoms/{symptomCode} Удаление правила признака

### Recommendation
- GET /recommendations оллекция правил рекоммендаций
- POST /recommendations Создание правила рекоммендации
- GET /recommendations/{recommendationCode} Информация о правиле рекоммендации
- PATCH /recommendations/{recommendationCode}Изменение правила рекоммендации
- DELETE /recommendations/{recommendationCode} Удаление правила рекоммендации

## Параметры запуска
static -c [путь до файла конфигурации]

## Конфигурация
* - обязательные параметры

_- значения по умолчанию
```
--------------------------------------------------------------------------------------------------------------------------------
| Параметр			  | Тип		| Описание													                           | VERSION 	| 	ENV					            | 
--------------------------------------------------------------------------------------------------------------------------------|
| http.host			  | string	| Адрес бинд-хоста HTTP интерфейса (0.0.0.0)	               | 0.1+		  |	DSS_HTTP_HOST		          |
| http.port			  | integer	| Порт бинд-хоста HTTP интерфейса (80)				               | 0.1+		  |	DSS_HTTP_PORT		          |
| *db.host			  | string	| Адрес хоста БД											                       | 0.1+		  |	DSS_DB_HOST			          |
| db.port			    | integer	| Порт хоста БД (27017)										                   | 0.1+		  |	DSS_DB_PORT			          |	
| db.login			  | string	| Логин пользователя БД										                   | 0.1+		  |	DSS_DB_LOGIN		          |
| db.password     | string	| Пароль пользователя БД									                   | 0.1+		  |	DSS_DB_PASSWORD		        |
| *db.database    | string	| Название БД												                         | 0.1+		  |	DSS_DB_DATABASE		        |
| prometheus.port | integer	| Порт метрик (9090)										                     | 0.2+		  |	DSS_PROMETHEUS_PORT	      |
| prometheus.path | string	| Путь до метрик (/metrics)									                 | 0.2+		  |	DSS_PROMETHEUS_PATH	      |
| sentryDSN	  		| string	| DSN аггрегатора ошибок									                   | 0.1+		  |	DSS_SENTRY_DSN   	        |
| logging.output 	| string	| Вывод ошибок в STDOUT или файл (указывается путь до файла) | 0.1+		  |	DSS_LOGGING_OUTPUT	      |
| logging.level		| string	| Уровень логирования (DEBUG|ERROR|FATAL|INFO)				       | 0.1+		  |	DSS_LOGGING_LEVEL	        |
| logging.format 	| string	| Формат логов (TEXT|JSON)									                 | 0.1+		  |	DSS_LOGGING_FORMAT	      |
| mq.host 			  | string	| Адрес брокера сооблщений									                 | 0.3+		  |	DSS_MQ_HOST			          |
| mq.port 			  | integer	| Порт брокера сообщений									                   | 0.3+		  |	DSS_MQ_PORT			          |
| mq.clientID 		| string	| Идентификатор клиента										                   | 0.3+		  |	DSS_MQ_CLIENTID		        |
| mq.login 			  | string	| Логин пользователя										                     | 0.3+		  |	DSS_MQ_LOGIN		          |
| mq.password 		| string	| Пароль пользователя										                     | 0.3+		  |	DSS_MQ_PASSWORD		        |
| mq.topic 			  | string	| Название очереди сообщений								                 | 0.3+		  |	DSS_MQ_TOPIC		          |
--------------------------------------------------------------------------------------------------------------------------------|
```
_____!
