package logger

import (
	"github.com/sirupsen/logrus"
	"io"
	"log"
	"strings"
)

type (
	coreStatus string
	apiStatus  string
)

const (
	CORESTARTED   coreStatus = "STARTED"
	CORECONNECTED coreStatus = "CONNECTED"
	COREFAILED    coreStatus = "FAILED"
	APICREATED    apiStatus  = "CREATED"
	APIUPDATED    apiStatus  = "UPDATED"
	APIDELETED    apiStatus  = "DELETED"
)

var logger Logger

func GetLogger() Logger {
	if logger == nil {
		log.Fatalln("Logger is not found")
	}
	return logger
}

// ====================================================

type Logger interface {
	Core() CoreEntrier
	Api() APIEntrier
	Query() QueryEntrier
}

type dssLog struct {
	lg *logrus.Logger
}

func NewLogger(out io.Writer, levStr string, format string) Logger {
	lg := logrus.New()
	level, err := logrus.ParseLevel(levStr)
	if err != nil {
		lg.WithError(err).Warnln("Can't set logging level")
	}
	lg.SetLevel(level)
	lg.SetOutput(out)
	if format == "JSON" {
		lg.SetFormatter(&logrus.JSONFormatter{})
	}

	logger = &dssLog{lg: lg}
	return logger
}

// ====================================================

type CoreEntrier interface {
	DSSInfo(resourse string, host string, port string, version string, status coreStatus)
	DSSFatal(resourse string, host string, port string, status coreStatus)
	DSSDebug(...interface{})
}

func (l *dssLog) Core() CoreEntrier {
	entry := l.lg.WithFields(logrus.Fields{
		"context": "CORE",
	})

	return &coreEntry{
		entry,
	}
}

type coreEntry struct {
	entry *logrus.Entry
}

func (e *coreEntry) DSSInfo(
	resourse string, host string, port string, version string, status coreStatus,
) {
	if resourse != "" {
		resourse = "resource=" + strings.ToUpper(resourse)
	}

	addr := host + ":" + port
	if addr != ":" {
		addr = " addr=" + strings.ToUpper(addr)
	} else {
		addr = ""
	}
	if version != "" {
		version = " version=" + strings.ToUpper(version)
	}
	if status != "" {
		status = " status=" + status
	}

	e.entry.Infof("%s%s%s%s", resourse, addr, version, status)
}

func (e *coreEntry) DSSFatal(resourse string, host string, port string, status coreStatus) {
	addr := host + ":" + port
	e.entry.Fatalf("resource=%s addr=%s status=%s",
		strings.ToUpper(resourse),
		strings.ToUpper(addr),
		status,
	)
}

func (e *coreEntry) DSSDebug(val ...interface{}) {
	e.entry.Debugln(val...)
}

// ====================================================

type APIEntrier interface {
	Patient() APILogger
	Symptom() APILogger
	Recommendation() APILogger
}

func (l *dssLog) Api() APIEntrier {
	entry := l.lg.WithFields(logrus.Fields{
		"context": "API",
	})
	return &apiEntry{
		entry,
	}
}

type apiEntry struct {
	entry *logrus.Entry
}

type APILogger interface {
	DSSInfo(guid string, status apiStatus)
	DSSDebug(...interface{})
}

type endpointEntry struct {
	entry     *apiEntry
	whichGUID string
}

func (e *apiEntry) Patient() APILogger {
	return &endpointEntry{
		e,
		"patientGUID=",
	}
}

func (e *apiEntry) Symptom() APILogger {
	return &endpointEntry{
		e,
		"symptomGUID=",
	}
}

func (e *apiEntry) Recommendation() APILogger {
	return &endpointEntry{
		e,
		"recommendationGUID=",
	}
}

func (e *endpointEntry) DSSInfo(guid string, status apiStatus) {
	e.entry.entry.Infof("%s%s status=%v",
		e.whichGUID,
		strings.ToUpper(guid),
		status,
	)
}

func (e *endpointEntry) DSSDebug(val ...interface{}) {
	e.entry.entry.Debugln(val...)
}

// ====================================================

func (l *dssLog) Query() QueryEntrier {
	entry := l.lg.WithFields(logrus.Fields{
		"context": "QUERY",
	})
	return &queryEntry{
		entry,
	}
}

type QueryEntrier interface {
	DSSInfo(string, string, string)
}

type queryEntry struct {
	*logrus.Entry
}

func (e *queryEntry) DSSInfo(patGUID string, group string, recommendGUID string) {
	e.WithField("type", "RECOMMENDATION").
		Infof("patientGUID=%s group=%s recommendationGUID=%s",
			strings.ToUpper(patGUID),
			strings.ToUpper(group),
			strings.ToUpper(recommendGUID),
		)
}

// ====================================================
